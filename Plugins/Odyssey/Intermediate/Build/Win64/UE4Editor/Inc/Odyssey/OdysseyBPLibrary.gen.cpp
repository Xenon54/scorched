// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Odyssey/Public/OdysseyBPLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOdysseyBPLibrary() {}
// Cross Module References
	ODYSSEY_API UClass* Z_Construct_UClass_UOdysseyBPLibrary_NoRegister();
	ODYSSEY_API UClass* Z_Construct_UClass_UOdysseyBPLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_Odyssey();
	ODYSSEY_API UFunction* Z_Construct_UFunction_UOdysseyBPLibrary_ClearDirectory();
	ODYSSEY_API UFunction* Z_Construct_UFunction_UOdysseyBPLibrary_DayInWeek();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FDateTime();
	ODYSSEY_API UFunction* Z_Construct_UFunction_UOdysseyBPLibrary_DistanceToString();
	ODYSSEY_API UFunction* Z_Construct_UFunction_UOdysseyBPLibrary_ExplodeString();
	ODYSSEY_API UFunction* Z_Construct_UFunction_UOdysseyBPLibrary_FindNearestVertexLocationInRange();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	ENGINE_API UEnum* Z_Construct_UEnum_Engine_EObjectTypeQuery();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	ODYSSEY_API UFunction* Z_Construct_UFunction_UOdysseyBPLibrary_FindOrSpawn();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	ODYSSEY_API UFunction* Z_Construct_UFunction_UOdysseyBPLibrary_FindOrSpawnTransform();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	ODYSSEY_API UFunction* Z_Construct_UFunction_UOdysseyBPLibrary_GetChildrenOfWidgetClass();
	UMG_API UClass* Z_Construct_UClass_UUserWidget_NoRegister();
	UMG_API UClass* Z_Construct_UClass_UWidget_NoRegister();
	ODYSSEY_API UFunction* Z_Construct_UFunction_UOdysseyBPLibrary_GetDeviceBatteryLevel();
	ODYSSEY_API UFunction* Z_Construct_UFunction_UOdysseyBPLibrary_GetDeviceOrientation();
	ODYSSEY_API UFunction* Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstActorOfClass();
	ODYSSEY_API UFunction* Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstActorOfClassPure();
	ODYSSEY_API UFunction* Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstWidgetOfClass();
	ODYSSEY_API UFunction* Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstWidgetOfClassPure();
	ODYSSEY_API UFunction* Z_Construct_UFunction_UOdysseyBPLibrary_GetMeshVertexes();
	ENGINE_API UClass* Z_Construct_UClass_UPrimitiveComponent_NoRegister();
	ODYSSEY_API UFunction* Z_Construct_UFunction_UOdysseyBPLibrary_GetOverlappingWidgets();
	ODYSSEY_API UFunction* Z_Construct_UFunction_UOdysseyBPLibrary_GetSubPaths();
	ODYSSEY_API UFunction* Z_Construct_UFunction_UOdysseyBPLibrary_IsDeviceCharging();
	ODYSSEY_API UFunction* Z_Construct_UFunction_UOdysseyBPLibrary_IsLeapYear();
	ODYSSEY_API UFunction* Z_Construct_UFunction_UOdysseyBPLibrary_LoadTexture2DFromFile();
	ENGINE_API UClass* Z_Construct_UClass_UTexture2D_NoRegister();
	ODYSSEY_API UFunction* Z_Construct_UFunction_UOdysseyBPLibrary_ProjectSavedFolder();
	ODYSSEY_API UFunction* Z_Construct_UFunction_UOdysseyBPLibrary_ScreenShotWithName();
	ODYSSEY_API UFunction* Z_Construct_UFunction_UOdysseyBPLibrary_SetMousePosition();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
	ENGINE_API UClass* Z_Construct_UClass_APlayerController_NoRegister();
	ODYSSEY_API UFunction* Z_Construct_UFunction_UOdysseyBPLibrary_ThumbnailFromSceneCaptureComponent();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FLinearColor();
	ENGINE_API UClass* Z_Construct_UClass_USceneCaptureComponent2D_NoRegister();
	ODYSSEY_API UFunction* Z_Construct_UFunction_UOdysseyBPLibrary_UserDesktopFolder();
// End Cross Module References
	void UOdysseyBPLibrary::StaticRegisterNativesUOdysseyBPLibrary()
	{
		UClass* Class = UOdysseyBPLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ClearDirectory", &UOdysseyBPLibrary::execClearDirectory },
			{ "DayInWeek", &UOdysseyBPLibrary::execDayInWeek },
			{ "DistanceToString", &UOdysseyBPLibrary::execDistanceToString },
			{ "ExplodeString", &UOdysseyBPLibrary::execExplodeString },
			{ "FindNearestVertexLocationInRange", &UOdysseyBPLibrary::execFindNearestVertexLocationInRange },
			{ "FindOrSpawn", &UOdysseyBPLibrary::execFindOrSpawn },
			{ "FindOrSpawnTransform", &UOdysseyBPLibrary::execFindOrSpawnTransform },
			{ "GetChildrenOfWidgetClass", &UOdysseyBPLibrary::execGetChildrenOfWidgetClass },
			{ "GetDeviceBatteryLevel", &UOdysseyBPLibrary::execGetDeviceBatteryLevel },
			{ "GetDeviceOrientation", &UOdysseyBPLibrary::execGetDeviceOrientation },
			{ "GetFirstActorOfClass", &UOdysseyBPLibrary::execGetFirstActorOfClass },
			{ "GetFirstActorOfClassPure", &UOdysseyBPLibrary::execGetFirstActorOfClassPure },
			{ "GetFirstWidgetOfClass", &UOdysseyBPLibrary::execGetFirstWidgetOfClass },
			{ "GetFirstWidgetOfClassPure", &UOdysseyBPLibrary::execGetFirstWidgetOfClassPure },
			{ "GetMeshVertexes", &UOdysseyBPLibrary::execGetMeshVertexes },
			{ "GetOverlappingWidgets", &UOdysseyBPLibrary::execGetOverlappingWidgets },
			{ "GetSubPaths", &UOdysseyBPLibrary::execGetSubPaths },
			{ "IsDeviceCharging", &UOdysseyBPLibrary::execIsDeviceCharging },
			{ "IsLeapYear", &UOdysseyBPLibrary::execIsLeapYear },
			{ "LoadTexture2DFromFile", &UOdysseyBPLibrary::execLoadTexture2DFromFile },
			{ "ProjectSavedFolder", &UOdysseyBPLibrary::execProjectSavedFolder },
			{ "ScreenShotWithName", &UOdysseyBPLibrary::execScreenShotWithName },
			{ "SetMousePosition", &UOdysseyBPLibrary::execSetMousePosition },
			{ "ThumbnailFromSceneCaptureComponent", &UOdysseyBPLibrary::execThumbnailFromSceneCaptureComponent },
			{ "UserDesktopFolder", &UOdysseyBPLibrary::execUserDesktopFolder },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UOdysseyBPLibrary_ClearDirectory_Statics
	{
		struct OdysseyBPLibrary_eventClearDirectory_Parms
		{
			FString Directory;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Directory_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Directory;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UOdysseyBPLibrary_ClearDirectory_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((OdysseyBPLibrary_eventClearDirectory_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_ClearDirectory_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(OdysseyBPLibrary_eventClearDirectory_Parms), &Z_Construct_UFunction_UOdysseyBPLibrary_ClearDirectory_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOdysseyBPLibrary_ClearDirectory_Statics::NewProp_Directory_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_ClearDirectory_Statics::NewProp_Directory = { UE4CodeGen_Private::EPropertyClass::Str, "Directory", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventClearDirectory_Parms, Directory), METADATA_PARAMS(Z_Construct_UFunction_UOdysseyBPLibrary_ClearDirectory_Statics::NewProp_Directory_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_ClearDirectory_Statics::NewProp_Directory_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOdysseyBPLibrary_ClearDirectory_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_ClearDirectory_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_ClearDirectory_Statics::NewProp_Directory,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOdysseyBPLibrary_ClearDirectory_Statics::Function_MetaDataParams[] = {
		{ "Category", "Directories" },
		{ "ModuleRelativePath", "Public/OdysseyBPLibrary.h" },
		{ "ToolTip", "Clears anything located in the folder, USE WITH CAUTION!" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOdysseyBPLibrary_ClearDirectory_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOdysseyBPLibrary, "ClearDirectory", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(OdysseyBPLibrary_eventClearDirectory_Parms), Z_Construct_UFunction_UOdysseyBPLibrary_ClearDirectory_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_ClearDirectory_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOdysseyBPLibrary_ClearDirectory_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_ClearDirectory_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOdysseyBPLibrary_ClearDirectory()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOdysseyBPLibrary_ClearDirectory_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOdysseyBPLibrary_DayInWeek_Statics
	{
		struct OdysseyBPLibrary_eventDayInWeek_Parms
		{
			FDateTime Date;
			uint8 ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Date;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_DayInWeek_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Byte, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventDayInWeek_Parms, ReturnValue), nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_DayInWeek_Statics::NewProp_Date = { UE4CodeGen_Private::EPropertyClass::Struct, "Date", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventDayInWeek_Parms, Date), Z_Construct_UScriptStruct_FDateTime, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOdysseyBPLibrary_DayInWeek_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_DayInWeek_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_DayInWeek_Statics::NewProp_Date,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOdysseyBPLibrary_DayInWeek_Statics::Function_MetaDataParams[] = {
		{ "Category", "Dates" },
		{ "ModuleRelativePath", "Public/OdysseyBPLibrary.h" },
		{ "ToolTip", "Gets the day of the week for a given date, 0 is Monday, 6 is Sunday" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOdysseyBPLibrary_DayInWeek_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOdysseyBPLibrary, "DayInWeek", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14822401, sizeof(OdysseyBPLibrary_eventDayInWeek_Parms), Z_Construct_UFunction_UOdysseyBPLibrary_DayInWeek_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_DayInWeek_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOdysseyBPLibrary_DayInWeek_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_DayInWeek_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOdysseyBPLibrary_DayInWeek()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOdysseyBPLibrary_DayInWeek_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOdysseyBPLibrary_DistanceToString_Statics
	{
		struct OdysseyBPLibrary_eventDistanceToString_Parms
		{
			float Value;
			int32 Exactness;
			bool bInRetardedUnits;
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static void NewProp_bInRetardedUnits_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInRetardedUnits;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Exactness;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_DistanceToString_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Str, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventDistanceToString_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UOdysseyBPLibrary_DistanceToString_Statics::NewProp_bInRetardedUnits_SetBit(void* Obj)
	{
		((OdysseyBPLibrary_eventDistanceToString_Parms*)Obj)->bInRetardedUnits = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_DistanceToString_Statics::NewProp_bInRetardedUnits = { UE4CodeGen_Private::EPropertyClass::Bool, "bInRetardedUnits", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(OdysseyBPLibrary_eventDistanceToString_Parms), &Z_Construct_UFunction_UOdysseyBPLibrary_DistanceToString_Statics::NewProp_bInRetardedUnits_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_DistanceToString_Statics::NewProp_Exactness = { UE4CodeGen_Private::EPropertyClass::Int, "Exactness", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventDistanceToString_Parms, Exactness), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_DistanceToString_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Float, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventDistanceToString_Parms, Value), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOdysseyBPLibrary_DistanceToString_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_DistanceToString_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_DistanceToString_Statics::NewProp_bInRetardedUnits,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_DistanceToString_Statics::NewProp_Exactness,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_DistanceToString_Statics::NewProp_Value,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOdysseyBPLibrary_DistanceToString_Statics::Function_MetaDataParams[] = {
		{ "Category", "String" },
		{ "CPP_Default_bInRetardedUnits", "false" },
		{ "CPP_Default_Exactness", "2" },
		{ "Keywords", "Get String with Units for distance." },
		{ "ModuleRelativePath", "Public/OdysseyBPLibrary.h" },
		{ "ToolTip", "Converts a raw float value to a string witch m, cm or km appended depending on the length." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOdysseyBPLibrary_DistanceToString_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOdysseyBPLibrary, "DistanceToString", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(OdysseyBPLibrary_eventDistanceToString_Parms), Z_Construct_UFunction_UOdysseyBPLibrary_DistanceToString_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_DistanceToString_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOdysseyBPLibrary_DistanceToString_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_DistanceToString_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOdysseyBPLibrary_DistanceToString()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOdysseyBPLibrary_DistanceToString_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOdysseyBPLibrary_ExplodeString_Statics
	{
		struct OdysseyBPLibrary_eventExplodeString_Parms
		{
			TArray<FString> OutputStrings;
			FString InputString;
			FString Separator;
			int32 limit;
			bool bTrimElements;
		};
		static void NewProp_bTrimElements_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bTrimElements;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_limit;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Separator;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_InputString;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_OutputStrings;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_OutputStrings_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UOdysseyBPLibrary_ExplodeString_Statics::NewProp_bTrimElements_SetBit(void* Obj)
	{
		((OdysseyBPLibrary_eventExplodeString_Parms*)Obj)->bTrimElements = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_ExplodeString_Statics::NewProp_bTrimElements = { UE4CodeGen_Private::EPropertyClass::Bool, "bTrimElements", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(OdysseyBPLibrary_eventExplodeString_Parms), &Z_Construct_UFunction_UOdysseyBPLibrary_ExplodeString_Statics::NewProp_bTrimElements_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_ExplodeString_Statics::NewProp_limit = { UE4CodeGen_Private::EPropertyClass::Int, "limit", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventExplodeString_Parms, limit), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_ExplodeString_Statics::NewProp_Separator = { UE4CodeGen_Private::EPropertyClass::Str, "Separator", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventExplodeString_Parms, Separator), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_ExplodeString_Statics::NewProp_InputString = { UE4CodeGen_Private::EPropertyClass::Str, "InputString", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventExplodeString_Parms, InputString), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_ExplodeString_Statics::NewProp_OutputStrings = { UE4CodeGen_Private::EPropertyClass::Array, "OutputStrings", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventExplodeString_Parms, OutputStrings), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_ExplodeString_Statics::NewProp_OutputStrings_Inner = { UE4CodeGen_Private::EPropertyClass::Str, "OutputStrings", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOdysseyBPLibrary_ExplodeString_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_ExplodeString_Statics::NewProp_bTrimElements,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_ExplodeString_Statics::NewProp_limit,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_ExplodeString_Statics::NewProp_Separator,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_ExplodeString_Statics::NewProp_InputString,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_ExplodeString_Statics::NewProp_OutputStrings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_ExplodeString_Statics::NewProp_OutputStrings_Inner,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOdysseyBPLibrary_ExplodeString_Statics::Function_MetaDataParams[] = {
		{ "Category", "String" },
		{ "CPP_Default_bTrimElements", "false" },
		{ "CPP_Default_limit", "0" },
		{ "CPP_Default_Separator", "," },
		{ "DisplayName", "Get Strings With Separator" },
		{ "Keywords", "Get Strings With Separator" },
		{ "ModuleRelativePath", "Public/OdysseyBPLibrary.h" },
		{ "ToolTip", "Converts a formatted string with separator into different strings without separator." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOdysseyBPLibrary_ExplodeString_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOdysseyBPLibrary, "ExplodeString", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14422401, sizeof(OdysseyBPLibrary_eventExplodeString_Parms), Z_Construct_UFunction_UOdysseyBPLibrary_ExplodeString_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_ExplodeString_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOdysseyBPLibrary_ExplodeString_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_ExplodeString_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOdysseyBPLibrary_ExplodeString()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOdysseyBPLibrary_ExplodeString_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOdysseyBPLibrary_FindNearestVertexLocationInRange_Statics
	{
		struct OdysseyBPLibrary_eventFindNearestVertexLocationInRange_Parms
		{
			UObject* WorldContextObject;
			FVector Location;
			float Range;
			TArray<TEnumAsByte<EObjectTypeQuery> > ObjectTypes;
			TArray<AActor*> ActorsToIgnore;
			FVector VertexPosition;
			AActor* HostActor;
			bool Found;
		};
		static void NewProp_Found_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Found;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_HostActor;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_VertexPosition;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActorsToIgnore_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ActorsToIgnore;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ActorsToIgnore_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ObjectTypes_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ObjectTypes;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ObjectTypes_Inner;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Range;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Location;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UOdysseyBPLibrary_FindNearestVertexLocationInRange_Statics::NewProp_Found_SetBit(void* Obj)
	{
		((OdysseyBPLibrary_eventFindNearestVertexLocationInRange_Parms*)Obj)->Found = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_FindNearestVertexLocationInRange_Statics::NewProp_Found = { UE4CodeGen_Private::EPropertyClass::Bool, "Found", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(OdysseyBPLibrary_eventFindNearestVertexLocationInRange_Parms), &Z_Construct_UFunction_UOdysseyBPLibrary_FindNearestVertexLocationInRange_Statics::NewProp_Found_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_FindNearestVertexLocationInRange_Statics::NewProp_HostActor = { UE4CodeGen_Private::EPropertyClass::Object, "HostActor", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventFindNearestVertexLocationInRange_Parms, HostActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_FindNearestVertexLocationInRange_Statics::NewProp_VertexPosition = { UE4CodeGen_Private::EPropertyClass::Struct, "VertexPosition", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventFindNearestVertexLocationInRange_Parms, VertexPosition), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOdysseyBPLibrary_FindNearestVertexLocationInRange_Statics::NewProp_ActorsToIgnore_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_FindNearestVertexLocationInRange_Statics::NewProp_ActorsToIgnore = { UE4CodeGen_Private::EPropertyClass::Array, "ActorsToIgnore", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000008000182, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventFindNearestVertexLocationInRange_Parms, ActorsToIgnore), METADATA_PARAMS(Z_Construct_UFunction_UOdysseyBPLibrary_FindNearestVertexLocationInRange_Statics::NewProp_ActorsToIgnore_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_FindNearestVertexLocationInRange_Statics::NewProp_ActorsToIgnore_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_FindNearestVertexLocationInRange_Statics::NewProp_ActorsToIgnore_Inner = { UE4CodeGen_Private::EPropertyClass::Object, "ActorsToIgnore", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOdysseyBPLibrary_FindNearestVertexLocationInRange_Statics::NewProp_ObjectTypes_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_FindNearestVertexLocationInRange_Statics::NewProp_ObjectTypes = { UE4CodeGen_Private::EPropertyClass::Array, "ObjectTypes", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventFindNearestVertexLocationInRange_Parms, ObjectTypes), METADATA_PARAMS(Z_Construct_UFunction_UOdysseyBPLibrary_FindNearestVertexLocationInRange_Statics::NewProp_ObjectTypes_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_FindNearestVertexLocationInRange_Statics::NewProp_ObjectTypes_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_FindNearestVertexLocationInRange_Statics::NewProp_ObjectTypes_Inner = { UE4CodeGen_Private::EPropertyClass::Byte, "ObjectTypes", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, Z_Construct_UEnum_Engine_EObjectTypeQuery, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_FindNearestVertexLocationInRange_Statics::NewProp_Range = { UE4CodeGen_Private::EPropertyClass::Float, "Range", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventFindNearestVertexLocationInRange_Parms, Range), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_FindNearestVertexLocationInRange_Statics::NewProp_Location = { UE4CodeGen_Private::EPropertyClass::Struct, "Location", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventFindNearestVertexLocationInRange_Parms, Location), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_FindNearestVertexLocationInRange_Statics::NewProp_WorldContextObject = { UE4CodeGen_Private::EPropertyClass::Object, "WorldContextObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventFindNearestVertexLocationInRange_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOdysseyBPLibrary_FindNearestVertexLocationInRange_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_FindNearestVertexLocationInRange_Statics::NewProp_Found,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_FindNearestVertexLocationInRange_Statics::NewProp_HostActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_FindNearestVertexLocationInRange_Statics::NewProp_VertexPosition,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_FindNearestVertexLocationInRange_Statics::NewProp_ActorsToIgnore,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_FindNearestVertexLocationInRange_Statics::NewProp_ActorsToIgnore_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_FindNearestVertexLocationInRange_Statics::NewProp_ObjectTypes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_FindNearestVertexLocationInRange_Statics::NewProp_ObjectTypes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_FindNearestVertexLocationInRange_Statics::NewProp_Range,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_FindNearestVertexLocationInRange_Statics::NewProp_Location,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_FindNearestVertexLocationInRange_Statics::NewProp_WorldContextObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOdysseyBPLibrary_FindNearestVertexLocationInRange_Statics::Function_MetaDataParams[] = {
		{ "Category", "Utilities" },
		{ "Keywords", "Find Vertex Range" },
		{ "ModuleRelativePath", "Public/OdysseyBPLibrary.h" },
		{ "ToolTip", "Returns the location of the nearest vertex to the Input location with a range." },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOdysseyBPLibrary_FindNearestVertexLocationInRange_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOdysseyBPLibrary, "FindNearestVertexLocationInRange", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04C22401, sizeof(OdysseyBPLibrary_eventFindNearestVertexLocationInRange_Parms), Z_Construct_UFunction_UOdysseyBPLibrary_FindNearestVertexLocationInRange_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_FindNearestVertexLocationInRange_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOdysseyBPLibrary_FindNearestVertexLocationInRange_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_FindNearestVertexLocationInRange_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOdysseyBPLibrary_FindNearestVertexLocationInRange()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOdysseyBPLibrary_FindNearestVertexLocationInRange_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOdysseyBPLibrary_FindOrSpawn_Statics
	{
		struct OdysseyBPLibrary_eventFindOrSpawn_Parms
		{
			const UObject* WorldContextObject;
			TSubclassOf<AActor>  ActorClass;
			AActor* OutActor;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OutActor;
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_ActorClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WorldContextObject_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_FindOrSpawn_Statics::NewProp_OutActor = { UE4CodeGen_Private::EPropertyClass::Object, "OutActor", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventFindOrSpawn_Parms, OutActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_FindOrSpawn_Statics::NewProp_ActorClass = { UE4CodeGen_Private::EPropertyClass::Class, "ActorClass", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0014000000000080, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventFindOrSpawn_Parms, ActorClass), Z_Construct_UClass_AActor_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOdysseyBPLibrary_FindOrSpawn_Statics::NewProp_WorldContextObject_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_FindOrSpawn_Statics::NewProp_WorldContextObject = { UE4CodeGen_Private::EPropertyClass::Object, "WorldContextObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventFindOrSpawn_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UOdysseyBPLibrary_FindOrSpawn_Statics::NewProp_WorldContextObject_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_FindOrSpawn_Statics::NewProp_WorldContextObject_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOdysseyBPLibrary_FindOrSpawn_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_FindOrSpawn_Statics::NewProp_OutActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_FindOrSpawn_Statics::NewProp_ActorClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_FindOrSpawn_Statics::NewProp_WorldContextObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOdysseyBPLibrary_FindOrSpawn_Statics::Function_MetaDataParams[] = {
		{ "Category", "Utilities" },
		{ "DeterminesOutputType", "ActorClass" },
		{ "DynamicOutputParam", "OutActor" },
		{ "ModuleRelativePath", "Public/OdysseyBPLibrary.h" },
		{ "ToolTip", "Try to find an object, if it fails, spawn it instead." },
		{ "UnsafeDuringActorConstruction", "" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOdysseyBPLibrary_FindOrSpawn_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOdysseyBPLibrary, "FindOrSpawn", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04422401, sizeof(OdysseyBPLibrary_eventFindOrSpawn_Parms), Z_Construct_UFunction_UOdysseyBPLibrary_FindOrSpawn_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_FindOrSpawn_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOdysseyBPLibrary_FindOrSpawn_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_FindOrSpawn_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOdysseyBPLibrary_FindOrSpawn()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOdysseyBPLibrary_FindOrSpawn_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOdysseyBPLibrary_FindOrSpawnTransform_Statics
	{
		struct OdysseyBPLibrary_eventFindOrSpawnTransform_Parms
		{
			const UObject* WorldContextObject;
			TSubclassOf<AActor>  ActorClass;
			AActor* OutActor;
			FTransform WithTransform;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WithTransform;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OutActor;
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_ActorClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WorldContextObject_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_FindOrSpawnTransform_Statics::NewProp_WithTransform = { UE4CodeGen_Private::EPropertyClass::Struct, "WithTransform", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventFindOrSpawnTransform_Parms, WithTransform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_FindOrSpawnTransform_Statics::NewProp_OutActor = { UE4CodeGen_Private::EPropertyClass::Object, "OutActor", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventFindOrSpawnTransform_Parms, OutActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_FindOrSpawnTransform_Statics::NewProp_ActorClass = { UE4CodeGen_Private::EPropertyClass::Class, "ActorClass", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0014000000000080, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventFindOrSpawnTransform_Parms, ActorClass), Z_Construct_UClass_AActor_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOdysseyBPLibrary_FindOrSpawnTransform_Statics::NewProp_WorldContextObject_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_FindOrSpawnTransform_Statics::NewProp_WorldContextObject = { UE4CodeGen_Private::EPropertyClass::Object, "WorldContextObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventFindOrSpawnTransform_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UOdysseyBPLibrary_FindOrSpawnTransform_Statics::NewProp_WorldContextObject_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_FindOrSpawnTransform_Statics::NewProp_WorldContextObject_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOdysseyBPLibrary_FindOrSpawnTransform_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_FindOrSpawnTransform_Statics::NewProp_WithTransform,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_FindOrSpawnTransform_Statics::NewProp_OutActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_FindOrSpawnTransform_Statics::NewProp_ActorClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_FindOrSpawnTransform_Statics::NewProp_WorldContextObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOdysseyBPLibrary_FindOrSpawnTransform_Statics::Function_MetaDataParams[] = {
		{ "Category", "Utilities" },
		{ "DeterminesOutputType", "ActorClass" },
		{ "DynamicOutputParam", "OutActor" },
		{ "ModuleRelativePath", "Public/OdysseyBPLibrary.h" },
		{ "ToolTip", "Try to find an object, if it fails, spawn it instead with added transform." },
		{ "UnsafeDuringActorConstruction", "" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOdysseyBPLibrary_FindOrSpawnTransform_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOdysseyBPLibrary, "FindOrSpawnTransform", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04C22401, sizeof(OdysseyBPLibrary_eventFindOrSpawnTransform_Parms), Z_Construct_UFunction_UOdysseyBPLibrary_FindOrSpawnTransform_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_FindOrSpawnTransform_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOdysseyBPLibrary_FindOrSpawnTransform_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_FindOrSpawnTransform_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOdysseyBPLibrary_FindOrSpawnTransform()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOdysseyBPLibrary_FindOrSpawnTransform_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOdysseyBPLibrary_GetChildrenOfWidgetClass_Statics
	{
		struct OdysseyBPLibrary_eventGetChildrenOfWidgetClass_Parms
		{
			UWidget* ParentWidget;
			TArray<UUserWidget*> ChildWidgets;
			TSubclassOf<UUserWidget>  WidgetClass;
			bool bImmediateOnly;
		};
		static void NewProp_bImmediateOnly_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bImmediateOnly;
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_WidgetClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChildWidgets_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ChildWidgets;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ChildWidgets_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ParentWidget_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ParentWidget;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UOdysseyBPLibrary_GetChildrenOfWidgetClass_Statics::NewProp_bImmediateOnly_SetBit(void* Obj)
	{
		((OdysseyBPLibrary_eventGetChildrenOfWidgetClass_Parms*)Obj)->bImmediateOnly = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_GetChildrenOfWidgetClass_Statics::NewProp_bImmediateOnly = { UE4CodeGen_Private::EPropertyClass::Bool, "bImmediateOnly", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(OdysseyBPLibrary_eventGetChildrenOfWidgetClass_Parms), &Z_Construct_UFunction_UOdysseyBPLibrary_GetChildrenOfWidgetClass_Statics::NewProp_bImmediateOnly_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_GetChildrenOfWidgetClass_Statics::NewProp_WidgetClass = { UE4CodeGen_Private::EPropertyClass::Class, "WidgetClass", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0014000000000080, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventGetChildrenOfWidgetClass_Parms, WidgetClass), Z_Construct_UClass_UUserWidget_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOdysseyBPLibrary_GetChildrenOfWidgetClass_Statics::NewProp_ChildWidgets_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_GetChildrenOfWidgetClass_Statics::NewProp_ChildWidgets = { UE4CodeGen_Private::EPropertyClass::Array, "ChildWidgets", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010008000000180, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventGetChildrenOfWidgetClass_Parms, ChildWidgets), METADATA_PARAMS(Z_Construct_UFunction_UOdysseyBPLibrary_GetChildrenOfWidgetClass_Statics::NewProp_ChildWidgets_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_GetChildrenOfWidgetClass_Statics::NewProp_ChildWidgets_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_GetChildrenOfWidgetClass_Statics::NewProp_ChildWidgets_Inner = { UE4CodeGen_Private::EPropertyClass::Object, "ChildWidgets", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000080000, 1, nullptr, 0, Z_Construct_UClass_UUserWidget_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOdysseyBPLibrary_GetChildrenOfWidgetClass_Statics::NewProp_ParentWidget_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_GetChildrenOfWidgetClass_Statics::NewProp_ParentWidget = { UE4CodeGen_Private::EPropertyClass::Object, "ParentWidget", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000080080, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventGetChildrenOfWidgetClass_Parms, ParentWidget), Z_Construct_UClass_UWidget_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UOdysseyBPLibrary_GetChildrenOfWidgetClass_Statics::NewProp_ParentWidget_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_GetChildrenOfWidgetClass_Statics::NewProp_ParentWidget_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOdysseyBPLibrary_GetChildrenOfWidgetClass_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_GetChildrenOfWidgetClass_Statics::NewProp_bImmediateOnly,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_GetChildrenOfWidgetClass_Statics::NewProp_WidgetClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_GetChildrenOfWidgetClass_Statics::NewProp_ChildWidgets,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_GetChildrenOfWidgetClass_Statics::NewProp_ChildWidgets_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_GetChildrenOfWidgetClass_Statics::NewProp_ParentWidget,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOdysseyBPLibrary_GetChildrenOfWidgetClass_Statics::Function_MetaDataParams[] = {
		{ "Category", "Widget" },
		{ "DefaultToSelf", "ParentWidget" },
		{ "DeterminesOutputType", "WidgetClass" },
		{ "DynamicOutputParam", "ChildWidgets" },
		{ "ModuleRelativePath", "Public/OdysseyBPLibrary.h" },
		{ "ToolTip", "Returns the children of the requested class" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOdysseyBPLibrary_GetChildrenOfWidgetClass_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOdysseyBPLibrary, "GetChildrenOfWidgetClass", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04422409, sizeof(OdysseyBPLibrary_eventGetChildrenOfWidgetClass_Parms), Z_Construct_UFunction_UOdysseyBPLibrary_GetChildrenOfWidgetClass_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_GetChildrenOfWidgetClass_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOdysseyBPLibrary_GetChildrenOfWidgetClass_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_GetChildrenOfWidgetClass_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOdysseyBPLibrary_GetChildrenOfWidgetClass()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOdysseyBPLibrary_GetChildrenOfWidgetClass_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOdysseyBPLibrary_GetDeviceBatteryLevel_Statics
	{
		struct OdysseyBPLibrary_eventGetDeviceBatteryLevel_Parms
		{
			uint8 ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_GetDeviceBatteryLevel_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Byte, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventGetDeviceBatteryLevel_Parms, ReturnValue), nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOdysseyBPLibrary_GetDeviceBatteryLevel_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_GetDeviceBatteryLevel_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOdysseyBPLibrary_GetDeviceBatteryLevel_Statics::Function_MetaDataParams[] = {
		{ "Category", "Mobile" },
		{ "ModuleRelativePath", "Public/OdysseyBPLibrary.h" },
		{ "ToolTip", "Gets the battery level, only works for Android/iOS" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOdysseyBPLibrary_GetDeviceBatteryLevel_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOdysseyBPLibrary, "GetDeviceBatteryLevel", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(OdysseyBPLibrary_eventGetDeviceBatteryLevel_Parms), Z_Construct_UFunction_UOdysseyBPLibrary_GetDeviceBatteryLevel_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_GetDeviceBatteryLevel_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOdysseyBPLibrary_GetDeviceBatteryLevel_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_GetDeviceBatteryLevel_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOdysseyBPLibrary_GetDeviceBatteryLevel()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOdysseyBPLibrary_GetDeviceBatteryLevel_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOdysseyBPLibrary_GetDeviceOrientation_Statics
	{
		struct OdysseyBPLibrary_eventGetDeviceOrientation_Parms
		{
			uint8 ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_GetDeviceOrientation_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Byte, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventGetDeviceOrientation_Parms, ReturnValue), nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOdysseyBPLibrary_GetDeviceOrientation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_GetDeviceOrientation_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOdysseyBPLibrary_GetDeviceOrientation_Statics::Function_MetaDataParams[] = {
		{ "Category", "Mobile" },
		{ "DeprecatedFunction", "" },
		{ "ModuleRelativePath", "Public/OdysseyBPLibrary.h" },
		{ "ToolTip", "0 is Unknown or unusable, 1 is portrait, 2 is landscape" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOdysseyBPLibrary_GetDeviceOrientation_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOdysseyBPLibrary, "GetDeviceOrientation", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(OdysseyBPLibrary_eventGetDeviceOrientation_Parms), Z_Construct_UFunction_UOdysseyBPLibrary_GetDeviceOrientation_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_GetDeviceOrientation_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOdysseyBPLibrary_GetDeviceOrientation_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_GetDeviceOrientation_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOdysseyBPLibrary_GetDeviceOrientation()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOdysseyBPLibrary_GetDeviceOrientation_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstActorOfClass_Statics
	{
		struct OdysseyBPLibrary_eventGetFirstActorOfClass_Parms
		{
			const UObject* WorldContextObject;
			TSubclassOf<AActor>  ActorClass;
			AActor* OutActor;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OutActor;
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_ActorClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WorldContextObject_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstActorOfClass_Statics::NewProp_OutActor = { UE4CodeGen_Private::EPropertyClass::Object, "OutActor", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventGetFirstActorOfClass_Parms, OutActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstActorOfClass_Statics::NewProp_ActorClass = { UE4CodeGen_Private::EPropertyClass::Class, "ActorClass", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0014000000000080, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventGetFirstActorOfClass_Parms, ActorClass), Z_Construct_UClass_AActor_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstActorOfClass_Statics::NewProp_WorldContextObject_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstActorOfClass_Statics::NewProp_WorldContextObject = { UE4CodeGen_Private::EPropertyClass::Object, "WorldContextObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventGetFirstActorOfClass_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstActorOfClass_Statics::NewProp_WorldContextObject_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstActorOfClass_Statics::NewProp_WorldContextObject_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstActorOfClass_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstActorOfClass_Statics::NewProp_OutActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstActorOfClass_Statics::NewProp_ActorClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstActorOfClass_Statics::NewProp_WorldContextObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstActorOfClass_Statics::Function_MetaDataParams[] = {
		{ "Category", "Utilities" },
		{ "DeterminesOutputType", "ActorClass" },
		{ "DynamicOutputParam", "OutActor" },
		{ "ModuleRelativePath", "Public/OdysseyBPLibrary.h" },
		{ "ToolTip", "Returns the first actor of requested class." },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstActorOfClass_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOdysseyBPLibrary, "GetFirstActorOfClass", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04422401, sizeof(OdysseyBPLibrary_eventGetFirstActorOfClass_Parms), Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstActorOfClass_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstActorOfClass_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstActorOfClass_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstActorOfClass_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstActorOfClass()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstActorOfClass_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstActorOfClassPure_Statics
	{
		struct OdysseyBPLibrary_eventGetFirstActorOfClassPure_Parms
		{
			const UObject* WorldContextObject;
			TSubclassOf<AActor>  ActorClass;
			AActor* OutActor;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OutActor;
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_ActorClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WorldContextObject_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstActorOfClassPure_Statics::NewProp_OutActor = { UE4CodeGen_Private::EPropertyClass::Object, "OutActor", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventGetFirstActorOfClassPure_Parms, OutActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstActorOfClassPure_Statics::NewProp_ActorClass = { UE4CodeGen_Private::EPropertyClass::Class, "ActorClass", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0014000000000080, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventGetFirstActorOfClassPure_Parms, ActorClass), Z_Construct_UClass_AActor_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstActorOfClassPure_Statics::NewProp_WorldContextObject_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstActorOfClassPure_Statics::NewProp_WorldContextObject = { UE4CodeGen_Private::EPropertyClass::Object, "WorldContextObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventGetFirstActorOfClassPure_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstActorOfClassPure_Statics::NewProp_WorldContextObject_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstActorOfClassPure_Statics::NewProp_WorldContextObject_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstActorOfClassPure_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstActorOfClassPure_Statics::NewProp_OutActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstActorOfClassPure_Statics::NewProp_ActorClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstActorOfClassPure_Statics::NewProp_WorldContextObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstActorOfClassPure_Statics::Function_MetaDataParams[] = {
		{ "Category", "Utilities" },
		{ "DeterminesOutputType", "ActorClass" },
		{ "DynamicOutputParam", "OutActor" },
		{ "ModuleRelativePath", "Public/OdysseyBPLibrary.h" },
		{ "ToolTip", "Returns the first actor of requested class. Pure version." },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstActorOfClassPure_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOdysseyBPLibrary, "GetFirstActorOfClassPure", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14422401, sizeof(OdysseyBPLibrary_eventGetFirstActorOfClassPure_Parms), Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstActorOfClassPure_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstActorOfClassPure_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstActorOfClassPure_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstActorOfClassPure_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstActorOfClassPure()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstActorOfClassPure_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstWidgetOfClass_Statics
	{
		struct OdysseyBPLibrary_eventGetFirstWidgetOfClass_Parms
		{
			UObject* WorldContextObject;
			TSubclassOf<UUserWidget>  WidgetClass;
			bool TopLevelOnly;
			UUserWidget* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static void NewProp_TopLevelOnly_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_TopLevelOnly;
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_WidgetClass;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstWidgetOfClass_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstWidgetOfClass_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000080588, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventGetFirstWidgetOfClass_Parms, ReturnValue), Z_Construct_UClass_UUserWidget_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstWidgetOfClass_Statics::NewProp_ReturnValue_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstWidgetOfClass_Statics::NewProp_ReturnValue_MetaData)) };
	void Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstWidgetOfClass_Statics::NewProp_TopLevelOnly_SetBit(void* Obj)
	{
		((OdysseyBPLibrary_eventGetFirstWidgetOfClass_Parms*)Obj)->TopLevelOnly = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstWidgetOfClass_Statics::NewProp_TopLevelOnly = { UE4CodeGen_Private::EPropertyClass::Bool, "TopLevelOnly", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(OdysseyBPLibrary_eventGetFirstWidgetOfClass_Parms), &Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstWidgetOfClass_Statics::NewProp_TopLevelOnly_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstWidgetOfClass_Statics::NewProp_WidgetClass = { UE4CodeGen_Private::EPropertyClass::Class, "WidgetClass", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0014000000000080, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventGetFirstWidgetOfClass_Parms, WidgetClass), Z_Construct_UClass_UUserWidget_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstWidgetOfClass_Statics::NewProp_WorldContextObject = { UE4CodeGen_Private::EPropertyClass::Object, "WorldContextObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventGetFirstWidgetOfClass_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstWidgetOfClass_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstWidgetOfClass_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstWidgetOfClass_Statics::NewProp_TopLevelOnly,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstWidgetOfClass_Statics::NewProp_WidgetClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstWidgetOfClass_Statics::NewProp_WorldContextObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstWidgetOfClass_Statics::Function_MetaDataParams[] = {
		{ "Category", "Widget" },
		{ "DeterminesOutputType", "WidgetClass" },
		{ "ModuleRelativePath", "Public/OdysseyBPLibrary.h" },
		{ "ToolTip", "Returns the first widget of the requested class" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstWidgetOfClass_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOdysseyBPLibrary, "GetFirstWidgetOfClass", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022409, sizeof(OdysseyBPLibrary_eventGetFirstWidgetOfClass_Parms), Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstWidgetOfClass_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstWidgetOfClass_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstWidgetOfClass_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstWidgetOfClass_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstWidgetOfClass()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstWidgetOfClass_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstWidgetOfClassPure_Statics
	{
		struct OdysseyBPLibrary_eventGetFirstWidgetOfClassPure_Parms
		{
			UObject* WorldContextObject;
			TSubclassOf<UUserWidget>  WidgetClass;
			bool TopLevelOnly;
			UUserWidget* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static void NewProp_TopLevelOnly_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_TopLevelOnly;
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_WidgetClass;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstWidgetOfClassPure_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstWidgetOfClassPure_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000080588, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventGetFirstWidgetOfClassPure_Parms, ReturnValue), Z_Construct_UClass_UUserWidget_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstWidgetOfClassPure_Statics::NewProp_ReturnValue_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstWidgetOfClassPure_Statics::NewProp_ReturnValue_MetaData)) };
	void Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstWidgetOfClassPure_Statics::NewProp_TopLevelOnly_SetBit(void* Obj)
	{
		((OdysseyBPLibrary_eventGetFirstWidgetOfClassPure_Parms*)Obj)->TopLevelOnly = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstWidgetOfClassPure_Statics::NewProp_TopLevelOnly = { UE4CodeGen_Private::EPropertyClass::Bool, "TopLevelOnly", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(OdysseyBPLibrary_eventGetFirstWidgetOfClassPure_Parms), &Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstWidgetOfClassPure_Statics::NewProp_TopLevelOnly_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstWidgetOfClassPure_Statics::NewProp_WidgetClass = { UE4CodeGen_Private::EPropertyClass::Class, "WidgetClass", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0014000000000080, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventGetFirstWidgetOfClassPure_Parms, WidgetClass), Z_Construct_UClass_UUserWidget_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstWidgetOfClassPure_Statics::NewProp_WorldContextObject = { UE4CodeGen_Private::EPropertyClass::Object, "WorldContextObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventGetFirstWidgetOfClassPure_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstWidgetOfClassPure_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstWidgetOfClassPure_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstWidgetOfClassPure_Statics::NewProp_TopLevelOnly,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstWidgetOfClassPure_Statics::NewProp_WidgetClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstWidgetOfClassPure_Statics::NewProp_WorldContextObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstWidgetOfClassPure_Statics::Function_MetaDataParams[] = {
		{ "Category", "Widget" },
		{ "DeterminesOutputType", "WidgetClass" },
		{ "ModuleRelativePath", "Public/OdysseyBPLibrary.h" },
		{ "ToolTip", "Returns the first widget of the requested class. Pure version" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstWidgetOfClassPure_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOdysseyBPLibrary, "GetFirstWidgetOfClassPure", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022409, sizeof(OdysseyBPLibrary_eventGetFirstWidgetOfClassPure_Parms), Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstWidgetOfClassPure_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstWidgetOfClassPure_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstWidgetOfClassPure_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstWidgetOfClassPure_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstWidgetOfClassPure()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstWidgetOfClassPure_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOdysseyBPLibrary_GetMeshVertexes_Statics
	{
		struct OdysseyBPLibrary_eventGetMeshVertexes_Parms
		{
			UPrimitiveComponent* Component;
			TArray<FVector> Vertexes;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Vertexes;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Vertexes_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Component_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Component;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UOdysseyBPLibrary_GetMeshVertexes_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((OdysseyBPLibrary_eventGetMeshVertexes_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_GetMeshVertexes_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(OdysseyBPLibrary_eventGetMeshVertexes_Parms), &Z_Construct_UFunction_UOdysseyBPLibrary_GetMeshVertexes_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_GetMeshVertexes_Statics::NewProp_Vertexes = { UE4CodeGen_Private::EPropertyClass::Array, "Vertexes", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventGetMeshVertexes_Parms, Vertexes), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_GetMeshVertexes_Statics::NewProp_Vertexes_Inner = { UE4CodeGen_Private::EPropertyClass::Struct, "Vertexes", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOdysseyBPLibrary_GetMeshVertexes_Statics::NewProp_Component_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_GetMeshVertexes_Statics::NewProp_Component = { UE4CodeGen_Private::EPropertyClass::Object, "Component", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000080080, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventGetMeshVertexes_Parms, Component), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UOdysseyBPLibrary_GetMeshVertexes_Statics::NewProp_Component_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_GetMeshVertexes_Statics::NewProp_Component_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOdysseyBPLibrary_GetMeshVertexes_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_GetMeshVertexes_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_GetMeshVertexes_Statics::NewProp_Vertexes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_GetMeshVertexes_Statics::NewProp_Vertexes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_GetMeshVertexes_Statics::NewProp_Component,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOdysseyBPLibrary_GetMeshVertexes_Statics::Function_MetaDataParams[] = {
		{ "Category", "Utilities" },
		{ "ModuleRelativePath", "Public/OdysseyBPLibrary.h" },
		{ "ToolTip", "Obtain the scaled,rotated, and translated vertex positions for any static mesh! Returns false if operation could not occur because the comp or static mesh asset was invalid." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOdysseyBPLibrary_GetMeshVertexes_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOdysseyBPLibrary, "GetMeshVertexes", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14422401, sizeof(OdysseyBPLibrary_eventGetMeshVertexes_Parms), Z_Construct_UFunction_UOdysseyBPLibrary_GetMeshVertexes_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_GetMeshVertexes_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOdysseyBPLibrary_GetMeshVertexes_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_GetMeshVertexes_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOdysseyBPLibrary_GetMeshVertexes()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOdysseyBPLibrary_GetMeshVertexes_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOdysseyBPLibrary_GetOverlappingWidgets_Statics
	{
		struct OdysseyBPLibrary_eventGetOverlappingWidgets_Parms
		{
			UObject* WorldContextObject;
			UWidget* Widget;
			TArray<UWidget*> InWidgets;
			TArray<UWidget*> ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InWidgets_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_InWidgets;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InWidgets_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Widget_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Widget;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOdysseyBPLibrary_GetOverlappingWidgets_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_GetOverlappingWidgets_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Array, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010008000000588, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventGetOverlappingWidgets_Parms, ReturnValue), METADATA_PARAMS(Z_Construct_UFunction_UOdysseyBPLibrary_GetOverlappingWidgets_Statics::NewProp_ReturnValue_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_GetOverlappingWidgets_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_GetOverlappingWidgets_Statics::NewProp_ReturnValue_Inner = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000080008, 1, nullptr, 0, Z_Construct_UClass_UWidget_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOdysseyBPLibrary_GetOverlappingWidgets_Statics::NewProp_InWidgets_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_GetOverlappingWidgets_Statics::NewProp_InWidgets = { UE4CodeGen_Private::EPropertyClass::Array, "InWidgets", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010008000000080, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventGetOverlappingWidgets_Parms, InWidgets), METADATA_PARAMS(Z_Construct_UFunction_UOdysseyBPLibrary_GetOverlappingWidgets_Statics::NewProp_InWidgets_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_GetOverlappingWidgets_Statics::NewProp_InWidgets_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_GetOverlappingWidgets_Statics::NewProp_InWidgets_Inner = { UE4CodeGen_Private::EPropertyClass::Object, "InWidgets", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000080000, 1, nullptr, 0, Z_Construct_UClass_UWidget_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOdysseyBPLibrary_GetOverlappingWidgets_Statics::NewProp_Widget_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_GetOverlappingWidgets_Statics::NewProp_Widget = { UE4CodeGen_Private::EPropertyClass::Object, "Widget", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000080080, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventGetOverlappingWidgets_Parms, Widget), Z_Construct_UClass_UWidget_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UOdysseyBPLibrary_GetOverlappingWidgets_Statics::NewProp_Widget_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_GetOverlappingWidgets_Statics::NewProp_Widget_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_GetOverlappingWidgets_Statics::NewProp_WorldContextObject = { UE4CodeGen_Private::EPropertyClass::Object, "WorldContextObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventGetOverlappingWidgets_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOdysseyBPLibrary_GetOverlappingWidgets_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_GetOverlappingWidgets_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_GetOverlappingWidgets_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_GetOverlappingWidgets_Statics::NewProp_InWidgets,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_GetOverlappingWidgets_Statics::NewProp_InWidgets_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_GetOverlappingWidgets_Statics::NewProp_Widget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_GetOverlappingWidgets_Statics::NewProp_WorldContextObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOdysseyBPLibrary_GetOverlappingWidgets_Statics::Function_MetaDataParams[] = {
		{ "Category", "Widget" },
		{ "DefaultToSelf", "WorldContextObject" },
		{ "HidePin", "WorldContextObject" },
		{ "ModuleRelativePath", "Public/OdysseyBPLibrary.h" },
		{ "ToolTip", "Returns all overlapping widgets" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOdysseyBPLibrary_GetOverlappingWidgets_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOdysseyBPLibrary, "GetOverlappingWidgets", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(OdysseyBPLibrary_eventGetOverlappingWidgets_Parms), Z_Construct_UFunction_UOdysseyBPLibrary_GetOverlappingWidgets_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_GetOverlappingWidgets_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOdysseyBPLibrary_GetOverlappingWidgets_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_GetOverlappingWidgets_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOdysseyBPLibrary_GetOverlappingWidgets()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOdysseyBPLibrary_GetOverlappingWidgets_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOdysseyBPLibrary_GetSubPaths_Statics
	{
		struct OdysseyBPLibrary_eventGetSubPaths_Parms
		{
			FString Directory;
			TArray<FString> SubPathsStripped;
		};
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SubPathsStripped;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SubPathsStripped_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Directory_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Directory;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_GetSubPaths_Statics::NewProp_SubPathsStripped = { UE4CodeGen_Private::EPropertyClass::Array, "SubPathsStripped", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventGetSubPaths_Parms, SubPathsStripped), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_GetSubPaths_Statics::NewProp_SubPathsStripped_Inner = { UE4CodeGen_Private::EPropertyClass::Str, "SubPathsStripped", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOdysseyBPLibrary_GetSubPaths_Statics::NewProp_Directory_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_GetSubPaths_Statics::NewProp_Directory = { UE4CodeGen_Private::EPropertyClass::Str, "Directory", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventGetSubPaths_Parms, Directory), METADATA_PARAMS(Z_Construct_UFunction_UOdysseyBPLibrary_GetSubPaths_Statics::NewProp_Directory_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_GetSubPaths_Statics::NewProp_Directory_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOdysseyBPLibrary_GetSubPaths_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_GetSubPaths_Statics::NewProp_SubPathsStripped,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_GetSubPaths_Statics::NewProp_SubPathsStripped_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_GetSubPaths_Statics::NewProp_Directory,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOdysseyBPLibrary_GetSubPaths_Statics::Function_MetaDataParams[] = {
		{ "Category", "Directories" },
		{ "ModuleRelativePath", "Public/OdysseyBPLibrary.h" },
		{ "ToolTip", "Returns a structured list of all subfolders in the called folder, will also return stripped folder names" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOdysseyBPLibrary_GetSubPaths_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOdysseyBPLibrary, "GetSubPaths", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04422401, sizeof(OdysseyBPLibrary_eventGetSubPaths_Parms), Z_Construct_UFunction_UOdysseyBPLibrary_GetSubPaths_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_GetSubPaths_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOdysseyBPLibrary_GetSubPaths_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_GetSubPaths_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOdysseyBPLibrary_GetSubPaths()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOdysseyBPLibrary_GetSubPaths_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOdysseyBPLibrary_IsDeviceCharging_Statics
	{
		struct OdysseyBPLibrary_eventIsDeviceCharging_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UOdysseyBPLibrary_IsDeviceCharging_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((OdysseyBPLibrary_eventIsDeviceCharging_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_IsDeviceCharging_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(OdysseyBPLibrary_eventIsDeviceCharging_Parms), &Z_Construct_UFunction_UOdysseyBPLibrary_IsDeviceCharging_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOdysseyBPLibrary_IsDeviceCharging_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_IsDeviceCharging_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOdysseyBPLibrary_IsDeviceCharging_Statics::Function_MetaDataParams[] = {
		{ "Category", "Mobile" },
		{ "ModuleRelativePath", "Public/OdysseyBPLibrary.h" },
		{ "ToolTip", "Returns if the mobile is charging" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOdysseyBPLibrary_IsDeviceCharging_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOdysseyBPLibrary, "IsDeviceCharging", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(OdysseyBPLibrary_eventIsDeviceCharging_Parms), Z_Construct_UFunction_UOdysseyBPLibrary_IsDeviceCharging_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_IsDeviceCharging_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOdysseyBPLibrary_IsDeviceCharging_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_IsDeviceCharging_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOdysseyBPLibrary_IsDeviceCharging()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOdysseyBPLibrary_IsDeviceCharging_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOdysseyBPLibrary_IsLeapYear_Statics
	{
		struct OdysseyBPLibrary_eventIsLeapYear_Parms
		{
			FDateTime Date;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Date;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UOdysseyBPLibrary_IsLeapYear_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((OdysseyBPLibrary_eventIsLeapYear_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_IsLeapYear_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(OdysseyBPLibrary_eventIsLeapYear_Parms), &Z_Construct_UFunction_UOdysseyBPLibrary_IsLeapYear_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_IsLeapYear_Statics::NewProp_Date = { UE4CodeGen_Private::EPropertyClass::Struct, "Date", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventIsLeapYear_Parms, Date), Z_Construct_UScriptStruct_FDateTime, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOdysseyBPLibrary_IsLeapYear_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_IsLeapYear_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_IsLeapYear_Statics::NewProp_Date,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOdysseyBPLibrary_IsLeapYear_Statics::Function_MetaDataParams[] = {
		{ "Category", "Dates" },
		{ "ModuleRelativePath", "Public/OdysseyBPLibrary.h" },
		{ "ToolTip", "Inputs a date and returns wheter or not it is a leap year" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOdysseyBPLibrary_IsLeapYear_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOdysseyBPLibrary, "IsLeapYear", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14822401, sizeof(OdysseyBPLibrary_eventIsLeapYear_Parms), Z_Construct_UFunction_UOdysseyBPLibrary_IsLeapYear_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_IsLeapYear_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOdysseyBPLibrary_IsLeapYear_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_IsLeapYear_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOdysseyBPLibrary_IsLeapYear()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOdysseyBPLibrary_IsLeapYear_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOdysseyBPLibrary_LoadTexture2DFromFile_Statics
	{
		struct OdysseyBPLibrary_eventLoadTexture2DFromFile_Parms
		{
			FString FullFilePath;
			bool IsValid;
			int32 Width;
			int32 Height;
			UTexture2D* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Height;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Width;
		static void NewProp_IsValid_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IsValid;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FullFilePath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FullFilePath;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_LoadTexture2DFromFile_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventLoadTexture2DFromFile_Parms, ReturnValue), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_LoadTexture2DFromFile_Statics::NewProp_Height = { UE4CodeGen_Private::EPropertyClass::Int, "Height", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventLoadTexture2DFromFile_Parms, Height), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_LoadTexture2DFromFile_Statics::NewProp_Width = { UE4CodeGen_Private::EPropertyClass::Int, "Width", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventLoadTexture2DFromFile_Parms, Width), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UOdysseyBPLibrary_LoadTexture2DFromFile_Statics::NewProp_IsValid_SetBit(void* Obj)
	{
		((OdysseyBPLibrary_eventLoadTexture2DFromFile_Parms*)Obj)->IsValid = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_LoadTexture2DFromFile_Statics::NewProp_IsValid = { UE4CodeGen_Private::EPropertyClass::Bool, "IsValid", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(OdysseyBPLibrary_eventLoadTexture2DFromFile_Parms), &Z_Construct_UFunction_UOdysseyBPLibrary_LoadTexture2DFromFile_Statics::NewProp_IsValid_SetBit, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOdysseyBPLibrary_LoadTexture2DFromFile_Statics::NewProp_FullFilePath_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_LoadTexture2DFromFile_Statics::NewProp_FullFilePath = { UE4CodeGen_Private::EPropertyClass::Str, "FullFilePath", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventLoadTexture2DFromFile_Parms, FullFilePath), METADATA_PARAMS(Z_Construct_UFunction_UOdysseyBPLibrary_LoadTexture2DFromFile_Statics::NewProp_FullFilePath_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_LoadTexture2DFromFile_Statics::NewProp_FullFilePath_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOdysseyBPLibrary_LoadTexture2DFromFile_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_LoadTexture2DFromFile_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_LoadTexture2DFromFile_Statics::NewProp_Height,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_LoadTexture2DFromFile_Statics::NewProp_Width,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_LoadTexture2DFromFile_Statics::NewProp_IsValid,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_LoadTexture2DFromFile_Statics::NewProp_FullFilePath,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOdysseyBPLibrary_LoadTexture2DFromFile_Statics::Function_MetaDataParams[] = {
		{ "Category", "Images" },
		{ "ModuleRelativePath", "Public/OdysseyBPLibrary.h" },
		{ "ToolTip", "Loads a texture from an absolute filepath." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOdysseyBPLibrary_LoadTexture2DFromFile_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOdysseyBPLibrary, "LoadTexture2DFromFile", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04422401, sizeof(OdysseyBPLibrary_eventLoadTexture2DFromFile_Parms), Z_Construct_UFunction_UOdysseyBPLibrary_LoadTexture2DFromFile_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_LoadTexture2DFromFile_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOdysseyBPLibrary_LoadTexture2DFromFile_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_LoadTexture2DFromFile_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOdysseyBPLibrary_LoadTexture2DFromFile()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOdysseyBPLibrary_LoadTexture2DFromFile_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOdysseyBPLibrary_ProjectSavedFolder_Statics
	{
		struct OdysseyBPLibrary_eventProjectSavedFolder_Parms
		{
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_ProjectSavedFolder_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Str, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventProjectSavedFolder_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOdysseyBPLibrary_ProjectSavedFolder_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_ProjectSavedFolder_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOdysseyBPLibrary_ProjectSavedFolder_Statics::Function_MetaDataParams[] = {
		{ "Category", "Directories" },
		{ "ModuleRelativePath", "Public/OdysseyBPLibrary.h" },
		{ "ToolTip", "Returns the location of the Saved folder (where images etc are stored)" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOdysseyBPLibrary_ProjectSavedFolder_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOdysseyBPLibrary, "ProjectSavedFolder", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(OdysseyBPLibrary_eventProjectSavedFolder_Parms), Z_Construct_UFunction_UOdysseyBPLibrary_ProjectSavedFolder_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_ProjectSavedFolder_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOdysseyBPLibrary_ProjectSavedFolder_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_ProjectSavedFolder_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOdysseyBPLibrary_ProjectSavedFolder()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOdysseyBPLibrary_ProjectSavedFolder_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOdysseyBPLibrary_ScreenShotWithName_Statics
	{
		struct OdysseyBPLibrary_eventScreenShotWithName_Parms
		{
			const UObject* WorldContextObject;
			FString Name;
			int32 ResolutionWidth;
			int32 ResolutionHeight;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ResolutionHeight;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ResolutionWidth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WorldContextObject_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_ScreenShotWithName_Statics::NewProp_ResolutionHeight = { UE4CodeGen_Private::EPropertyClass::Int, "ResolutionHeight", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventScreenShotWithName_Parms, ResolutionHeight), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_ScreenShotWithName_Statics::NewProp_ResolutionWidth = { UE4CodeGen_Private::EPropertyClass::Int, "ResolutionWidth", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventScreenShotWithName_Parms, ResolutionWidth), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOdysseyBPLibrary_ScreenShotWithName_Statics::NewProp_Name_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_ScreenShotWithName_Statics::NewProp_Name = { UE4CodeGen_Private::EPropertyClass::Str, "Name", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventScreenShotWithName_Parms, Name), METADATA_PARAMS(Z_Construct_UFunction_UOdysseyBPLibrary_ScreenShotWithName_Statics::NewProp_Name_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_ScreenShotWithName_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOdysseyBPLibrary_ScreenShotWithName_Statics::NewProp_WorldContextObject_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_ScreenShotWithName_Statics::NewProp_WorldContextObject = { UE4CodeGen_Private::EPropertyClass::Object, "WorldContextObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventScreenShotWithName_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UOdysseyBPLibrary_ScreenShotWithName_Statics::NewProp_WorldContextObject_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_ScreenShotWithName_Statics::NewProp_WorldContextObject_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOdysseyBPLibrary_ScreenShotWithName_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_ScreenShotWithName_Statics::NewProp_ResolutionHeight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_ScreenShotWithName_Statics::NewProp_ResolutionWidth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_ScreenShotWithName_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_ScreenShotWithName_Statics::NewProp_WorldContextObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOdysseyBPLibrary_ScreenShotWithName_Statics::Function_MetaDataParams[] = {
		{ "Category", "Images" },
		{ "ModuleRelativePath", "Public/OdysseyBPLibrary.h" },
		{ "ToolTip", "Takes a highres screenshot in the default location with a name." },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOdysseyBPLibrary_ScreenShotWithName_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOdysseyBPLibrary, "ScreenShotWithName", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(OdysseyBPLibrary_eventScreenShotWithName_Parms), Z_Construct_UFunction_UOdysseyBPLibrary_ScreenShotWithName_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_ScreenShotWithName_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOdysseyBPLibrary_ScreenShotWithName_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_ScreenShotWithName_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOdysseyBPLibrary_ScreenShotWithName()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOdysseyBPLibrary_ScreenShotWithName_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOdysseyBPLibrary_SetMousePosition_Statics
	{
		struct OdysseyBPLibrary_eventSetMousePosition_Parms
		{
			APlayerController* PlayerController;
			FVector2D NewPos;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_NewPos;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PlayerController;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UOdysseyBPLibrary_SetMousePosition_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((OdysseyBPLibrary_eventSetMousePosition_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_SetMousePosition_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(OdysseyBPLibrary_eventSetMousePosition_Parms), &Z_Construct_UFunction_UOdysseyBPLibrary_SetMousePosition_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_SetMousePosition_Statics::NewProp_NewPos = { UE4CodeGen_Private::EPropertyClass::Struct, "NewPos", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventSetMousePosition_Parms, NewPos), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_SetMousePosition_Statics::NewProp_PlayerController = { UE4CodeGen_Private::EPropertyClass::Object, "PlayerController", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventSetMousePosition_Parms, PlayerController), Z_Construct_UClass_APlayerController_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOdysseyBPLibrary_SetMousePosition_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_SetMousePosition_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_SetMousePosition_Statics::NewProp_NewPos,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_SetMousePosition_Statics::NewProp_PlayerController,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOdysseyBPLibrary_SetMousePosition_Statics::Function_MetaDataParams[] = {
		{ "Category", "Utilities" },
		{ "DisplayName", "Set Mouse Cursor Position" },
		{ "Keywords", "Mouse Cursor Position" },
		{ "ModuleRelativePath", "Public/OdysseyBPLibrary.h" },
		{ "ToolTip", "Sets the mouse position in screen space." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOdysseyBPLibrary_SetMousePosition_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOdysseyBPLibrary, "SetMousePosition", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04822401, sizeof(OdysseyBPLibrary_eventSetMousePosition_Parms), Z_Construct_UFunction_UOdysseyBPLibrary_SetMousePosition_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_SetMousePosition_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOdysseyBPLibrary_SetMousePosition_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_SetMousePosition_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOdysseyBPLibrary_SetMousePosition()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOdysseyBPLibrary_SetMousePosition_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOdysseyBPLibrary_ThumbnailFromSceneCaptureComponent_Statics
	{
		struct OdysseyBPLibrary_eventThumbnailFromSceneCaptureComponent_Parms
		{
			USceneCaptureComponent2D* SceneCaptureComponent;
			FString ImagePath;
			bool SRGB;
			bool bClearBackground;
			FLinearColor ClearColor;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ClearColor;
		static void NewProp_bClearBackground_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bClearBackground;
		static void NewProp_SRGB_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_SRGB;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ImagePath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ImagePath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SceneCaptureComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SceneCaptureComponent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UOdysseyBPLibrary_ThumbnailFromSceneCaptureComponent_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((OdysseyBPLibrary_eventThumbnailFromSceneCaptureComponent_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_ThumbnailFromSceneCaptureComponent_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(OdysseyBPLibrary_eventThumbnailFromSceneCaptureComponent_Parms), &Z_Construct_UFunction_UOdysseyBPLibrary_ThumbnailFromSceneCaptureComponent_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_ThumbnailFromSceneCaptureComponent_Statics::NewProp_ClearColor = { UE4CodeGen_Private::EPropertyClass::Struct, "ClearColor", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventThumbnailFromSceneCaptureComponent_Parms, ClearColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UOdysseyBPLibrary_ThumbnailFromSceneCaptureComponent_Statics::NewProp_bClearBackground_SetBit(void* Obj)
	{
		((OdysseyBPLibrary_eventThumbnailFromSceneCaptureComponent_Parms*)Obj)->bClearBackground = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_ThumbnailFromSceneCaptureComponent_Statics::NewProp_bClearBackground = { UE4CodeGen_Private::EPropertyClass::Bool, "bClearBackground", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(OdysseyBPLibrary_eventThumbnailFromSceneCaptureComponent_Parms), &Z_Construct_UFunction_UOdysseyBPLibrary_ThumbnailFromSceneCaptureComponent_Statics::NewProp_bClearBackground_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UOdysseyBPLibrary_ThumbnailFromSceneCaptureComponent_Statics::NewProp_SRGB_SetBit(void* Obj)
	{
		((OdysseyBPLibrary_eventThumbnailFromSceneCaptureComponent_Parms*)Obj)->SRGB = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_ThumbnailFromSceneCaptureComponent_Statics::NewProp_SRGB = { UE4CodeGen_Private::EPropertyClass::Bool, "SRGB", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(OdysseyBPLibrary_eventThumbnailFromSceneCaptureComponent_Parms), &Z_Construct_UFunction_UOdysseyBPLibrary_ThumbnailFromSceneCaptureComponent_Statics::NewProp_SRGB_SetBit, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOdysseyBPLibrary_ThumbnailFromSceneCaptureComponent_Statics::NewProp_ImagePath_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_ThumbnailFromSceneCaptureComponent_Statics::NewProp_ImagePath = { UE4CodeGen_Private::EPropertyClass::Str, "ImagePath", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventThumbnailFromSceneCaptureComponent_Parms, ImagePath), METADATA_PARAMS(Z_Construct_UFunction_UOdysseyBPLibrary_ThumbnailFromSceneCaptureComponent_Statics::NewProp_ImagePath_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_ThumbnailFromSceneCaptureComponent_Statics::NewProp_ImagePath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOdysseyBPLibrary_ThumbnailFromSceneCaptureComponent_Statics::NewProp_SceneCaptureComponent_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_ThumbnailFromSceneCaptureComponent_Statics::NewProp_SceneCaptureComponent = { UE4CodeGen_Private::EPropertyClass::Object, "SceneCaptureComponent", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000080080, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventThumbnailFromSceneCaptureComponent_Parms, SceneCaptureComponent), Z_Construct_UClass_USceneCaptureComponent2D_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UOdysseyBPLibrary_ThumbnailFromSceneCaptureComponent_Statics::NewProp_SceneCaptureComponent_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_ThumbnailFromSceneCaptureComponent_Statics::NewProp_SceneCaptureComponent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOdysseyBPLibrary_ThumbnailFromSceneCaptureComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_ThumbnailFromSceneCaptureComponent_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_ThumbnailFromSceneCaptureComponent_Statics::NewProp_ClearColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_ThumbnailFromSceneCaptureComponent_Statics::NewProp_bClearBackground,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_ThumbnailFromSceneCaptureComponent_Statics::NewProp_SRGB,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_ThumbnailFromSceneCaptureComponent_Statics::NewProp_ImagePath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_ThumbnailFromSceneCaptureComponent_Statics::NewProp_SceneCaptureComponent,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOdysseyBPLibrary_ThumbnailFromSceneCaptureComponent_Statics::Function_MetaDataParams[] = {
		{ "Category", "Images" },
		{ "CPP_Default_bClearBackground", "true" },
		{ "CPP_Default_ClearColor", "(R=1.000000,G=1.000000,B=1.000000,A=1.000000)" },
		{ "Keywords", "Thumbnail Capture" },
		{ "ModuleRelativePath", "Public/OdysseyBPLibrary.h" },
		{ "ToolTip", "Generates a thumbnail from a SceneCaptureComponent on an absolute location." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOdysseyBPLibrary_ThumbnailFromSceneCaptureComponent_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOdysseyBPLibrary, "ThumbnailFromSceneCaptureComponent", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04822401, sizeof(OdysseyBPLibrary_eventThumbnailFromSceneCaptureComponent_Parms), Z_Construct_UFunction_UOdysseyBPLibrary_ThumbnailFromSceneCaptureComponent_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_ThumbnailFromSceneCaptureComponent_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOdysseyBPLibrary_ThumbnailFromSceneCaptureComponent_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_ThumbnailFromSceneCaptureComponent_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOdysseyBPLibrary_ThumbnailFromSceneCaptureComponent()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOdysseyBPLibrary_ThumbnailFromSceneCaptureComponent_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOdysseyBPLibrary_UserDesktopFolder_Statics
	{
		struct OdysseyBPLibrary_eventUserDesktopFolder_Parms
		{
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UOdysseyBPLibrary_UserDesktopFolder_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Str, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(OdysseyBPLibrary_eventUserDesktopFolder_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOdysseyBPLibrary_UserDesktopFolder_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOdysseyBPLibrary_UserDesktopFolder_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOdysseyBPLibrary_UserDesktopFolder_Statics::Function_MetaDataParams[] = {
		{ "Category", "Directories" },
		{ "ModuleRelativePath", "Public/OdysseyBPLibrary.h" },
		{ "ToolTip", "Returns the location of the User Desktop folder" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOdysseyBPLibrary_UserDesktopFolder_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOdysseyBPLibrary, "UserDesktopFolder", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(OdysseyBPLibrary_eventUserDesktopFolder_Parms), Z_Construct_UFunction_UOdysseyBPLibrary_UserDesktopFolder_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_UserDesktopFolder_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOdysseyBPLibrary_UserDesktopFolder_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOdysseyBPLibrary_UserDesktopFolder_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOdysseyBPLibrary_UserDesktopFolder()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOdysseyBPLibrary_UserDesktopFolder_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UOdysseyBPLibrary_NoRegister()
	{
		return UOdysseyBPLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UOdysseyBPLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UOdysseyBPLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_Odyssey,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UOdysseyBPLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UOdysseyBPLibrary_ClearDirectory, "ClearDirectory" }, // 3895599872
		{ &Z_Construct_UFunction_UOdysseyBPLibrary_DayInWeek, "DayInWeek" }, // 1037468858
		{ &Z_Construct_UFunction_UOdysseyBPLibrary_DistanceToString, "DistanceToString" }, // 1849076789
		{ &Z_Construct_UFunction_UOdysseyBPLibrary_ExplodeString, "ExplodeString" }, // 3846629133
		{ &Z_Construct_UFunction_UOdysseyBPLibrary_FindNearestVertexLocationInRange, "FindNearestVertexLocationInRange" }, // 2878957094
		{ &Z_Construct_UFunction_UOdysseyBPLibrary_FindOrSpawn, "FindOrSpawn" }, // 3820990447
		{ &Z_Construct_UFunction_UOdysseyBPLibrary_FindOrSpawnTransform, "FindOrSpawnTransform" }, // 564370413
		{ &Z_Construct_UFunction_UOdysseyBPLibrary_GetChildrenOfWidgetClass, "GetChildrenOfWidgetClass" }, // 4118427761
		{ &Z_Construct_UFunction_UOdysseyBPLibrary_GetDeviceBatteryLevel, "GetDeviceBatteryLevel" }, // 128567798
		{ &Z_Construct_UFunction_UOdysseyBPLibrary_GetDeviceOrientation, "GetDeviceOrientation" }, // 3791861461
		{ &Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstActorOfClass, "GetFirstActorOfClass" }, // 556172821
		{ &Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstActorOfClassPure, "GetFirstActorOfClassPure" }, // 105954751
		{ &Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstWidgetOfClass, "GetFirstWidgetOfClass" }, // 2272683475
		{ &Z_Construct_UFunction_UOdysseyBPLibrary_GetFirstWidgetOfClassPure, "GetFirstWidgetOfClassPure" }, // 4056994106
		{ &Z_Construct_UFunction_UOdysseyBPLibrary_GetMeshVertexes, "GetMeshVertexes" }, // 141884445
		{ &Z_Construct_UFunction_UOdysseyBPLibrary_GetOverlappingWidgets, "GetOverlappingWidgets" }, // 2748850794
		{ &Z_Construct_UFunction_UOdysseyBPLibrary_GetSubPaths, "GetSubPaths" }, // 1281836905
		{ &Z_Construct_UFunction_UOdysseyBPLibrary_IsDeviceCharging, "IsDeviceCharging" }, // 1009901505
		{ &Z_Construct_UFunction_UOdysseyBPLibrary_IsLeapYear, "IsLeapYear" }, // 702592891
		{ &Z_Construct_UFunction_UOdysseyBPLibrary_LoadTexture2DFromFile, "LoadTexture2DFromFile" }, // 3252688049
		{ &Z_Construct_UFunction_UOdysseyBPLibrary_ProjectSavedFolder, "ProjectSavedFolder" }, // 1073608549
		{ &Z_Construct_UFunction_UOdysseyBPLibrary_ScreenShotWithName, "ScreenShotWithName" }, // 3369788265
		{ &Z_Construct_UFunction_UOdysseyBPLibrary_SetMousePosition, "SetMousePosition" }, // 1644850573
		{ &Z_Construct_UFunction_UOdysseyBPLibrary_ThumbnailFromSceneCaptureComponent, "ThumbnailFromSceneCaptureComponent" }, // 27335771
		{ &Z_Construct_UFunction_UOdysseyBPLibrary_UserDesktopFolder, "UserDesktopFolder" }, // 68727404
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOdysseyBPLibrary_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "OdysseyBPLibrary.h" },
		{ "ModuleRelativePath", "Public/OdysseyBPLibrary.h" },
		{ "ToolTip", "Function library class.\nEach function in it is expected to be static and represents blueprint node that can be called in any blueprint.\n\nWhen declaring function you can define metadata for the node. Key function specifiers will be BlueprintPure and BlueprintCallable.\nBlueprintPure - means the function does not affect the owning object in any way and thus creates a node without Exec pins.\nBlueprintCallable - makes a function which can be executed in Blueprints - Thus it has Exec pins.\nDisplayName - full name of the node, shown when you mouse over the node and in the blueprint drop down menu.\n                        Its lets you name the node using characters not allowed in C++ function names.\nCompactNodeTitle - the word(s) that appear on the node.\nKeywords -      the list of keywords that helps you to find node when you search for it using Blueprint drop-down menu.\n                        Good example is \"Print String\" node which you can find also by using keyword \"log\".\nCategory -      the category your node will be under in the Blueprint drop-down menu.\n\nFor more info on custom blueprint nodes visit documentation:\nhttps://wiki.unrealengine.com/Custom_Blueprint_Node_Creation" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UOdysseyBPLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UOdysseyBPLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UOdysseyBPLibrary_Statics::ClassParams = {
		&UOdysseyBPLibrary::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x000000A0u,
		FuncInfo, ARRAY_COUNT(FuncInfo),
		nullptr, 0,
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_UOdysseyBPLibrary_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UOdysseyBPLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UOdysseyBPLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UOdysseyBPLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UOdysseyBPLibrary, 3008769611);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UOdysseyBPLibrary(Z_Construct_UClass_UOdysseyBPLibrary, &UOdysseyBPLibrary::StaticClass, TEXT("/Script/Odyssey"), TEXT("UOdysseyBPLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UOdysseyBPLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
