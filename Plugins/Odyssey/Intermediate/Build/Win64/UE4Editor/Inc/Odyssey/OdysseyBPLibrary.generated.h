// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FDateTime;
class UObject;
class UWidget;
class UUserWidget;
class AActor;
struct FTransform;
class USceneCaptureComponent2D;
struct FLinearColor;
class UTexture2D;
class APlayerController;
struct FVector2D;
struct FVector;
class UPrimitiveComponent;
#ifdef ODYSSEY_OdysseyBPLibrary_generated_h
#error "OdysseyBPLibrary.generated.h already included, missing '#pragma once' in OdysseyBPLibrary.h"
#endif
#define ODYSSEY_OdysseyBPLibrary_generated_h

#define ScorchedSpaceVR_Plugins_Odyssey_Source_Odyssey_Public_OdysseyBPLibrary_h_40_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetDeviceOrientation) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(uint8*)Z_Param__Result=UOdysseyBPLibrary::GetDeviceOrientation(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIsDeviceCharging) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=UOdysseyBPLibrary::IsDeviceCharging(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetDeviceBatteryLevel) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(uint8*)Z_Param__Result=UOdysseyBPLibrary::GetDeviceBatteryLevel(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execDayInWeek) \
	{ \
		P_GET_STRUCT(FDateTime,Z_Param_Date); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(uint8*)Z_Param__Result=UOdysseyBPLibrary::DayInWeek(Z_Param_Date); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIsLeapYear) \
	{ \
		P_GET_STRUCT(FDateTime,Z_Param_Date); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=UOdysseyBPLibrary::IsLeapYear(Z_Param_Date); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetSubPaths) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_Directory); \
		P_GET_TARRAY_REF(FString,Z_Param_Out_SubPathsStripped); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		UOdysseyBPLibrary::GetSubPaths(Z_Param_Directory,Z_Param_Out_SubPathsStripped); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execClearDirectory) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_Directory); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=UOdysseyBPLibrary::ClearDirectory(Z_Param_Directory); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execUserDesktopFolder) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=UOdysseyBPLibrary::UserDesktopFolder(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execProjectSavedFolder) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=UOdysseyBPLibrary::ProjectSavedFolder(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetOverlappingWidgets) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject); \
		P_GET_OBJECT(UWidget,Z_Param_Widget); \
		P_GET_TARRAY(UWidget*,Z_Param_InWidgets); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(TArray<UWidget*>*)Z_Param__Result=UOdysseyBPLibrary::GetOverlappingWidgets(Z_Param_WorldContextObject,Z_Param_Widget,Z_Param_InWidgets); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetChildrenOfWidgetClass) \
	{ \
		P_GET_OBJECT(UWidget,Z_Param_ParentWidget); \
		P_GET_TARRAY_REF(UUserWidget*,Z_Param_Out_ChildWidgets); \
		P_GET_OBJECT(UClass,Z_Param_WidgetClass); \
		P_GET_UBOOL(Z_Param_bImmediateOnly); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		UOdysseyBPLibrary::GetChildrenOfWidgetClass(Z_Param_ParentWidget,Z_Param_Out_ChildWidgets,Z_Param_WidgetClass,Z_Param_bImmediateOnly); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetFirstWidgetOfClassPure) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject); \
		P_GET_OBJECT(UClass,Z_Param_WidgetClass); \
		P_GET_UBOOL(Z_Param_TopLevelOnly); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(UUserWidget**)Z_Param__Result=UOdysseyBPLibrary::GetFirstWidgetOfClassPure(Z_Param_WorldContextObject,Z_Param_WidgetClass,Z_Param_TopLevelOnly); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetFirstWidgetOfClass) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject); \
		P_GET_OBJECT(UClass,Z_Param_WidgetClass); \
		P_GET_UBOOL(Z_Param_TopLevelOnly); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(UUserWidget**)Z_Param__Result=UOdysseyBPLibrary::GetFirstWidgetOfClass(Z_Param_WorldContextObject,Z_Param_WidgetClass,Z_Param_TopLevelOnly); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execExplodeString) \
	{ \
		P_GET_TARRAY_REF(FString,Z_Param_Out_OutputStrings); \
		P_GET_PROPERTY(UStrProperty,Z_Param_InputString); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Separator); \
		P_GET_PROPERTY(UIntProperty,Z_Param_limit); \
		P_GET_UBOOL(Z_Param_bTrimElements); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		UOdysseyBPLibrary::ExplodeString(Z_Param_Out_OutputStrings,Z_Param_InputString,Z_Param_Separator,Z_Param_limit,Z_Param_bTrimElements); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execDistanceToString) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Value); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Exactness); \
		P_GET_UBOOL(Z_Param_bInRetardedUnits); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=UOdysseyBPLibrary::DistanceToString(Z_Param_Value,Z_Param_Exactness,Z_Param_bInRetardedUnits); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetFirstActorOfClassPure) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject); \
		P_GET_OBJECT(UClass,Z_Param_ActorClass); \
		P_GET_OBJECT_REF(AActor,Z_Param_Out_OutActor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		UOdysseyBPLibrary::GetFirstActorOfClassPure(Z_Param_WorldContextObject,Z_Param_ActorClass,Z_Param_Out_OutActor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetFirstActorOfClass) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject); \
		P_GET_OBJECT(UClass,Z_Param_ActorClass); \
		P_GET_OBJECT_REF(AActor,Z_Param_Out_OutActor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		UOdysseyBPLibrary::GetFirstActorOfClass(Z_Param_WorldContextObject,Z_Param_ActorClass,Z_Param_Out_OutActor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execFindOrSpawnTransform) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject); \
		P_GET_OBJECT(UClass,Z_Param_ActorClass); \
		P_GET_OBJECT_REF(AActor,Z_Param_Out_OutActor); \
		P_GET_STRUCT(FTransform,Z_Param_WithTransform); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		UOdysseyBPLibrary::FindOrSpawnTransform(Z_Param_WorldContextObject,Z_Param_ActorClass,Z_Param_Out_OutActor,Z_Param_WithTransform); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execFindOrSpawn) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject); \
		P_GET_OBJECT(UClass,Z_Param_ActorClass); \
		P_GET_OBJECT_REF(AActor,Z_Param_Out_OutActor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		UOdysseyBPLibrary::FindOrSpawn(Z_Param_WorldContextObject,Z_Param_ActorClass,Z_Param_Out_OutActor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execThumbnailFromSceneCaptureComponent) \
	{ \
		P_GET_OBJECT(USceneCaptureComponent2D,Z_Param_SceneCaptureComponent); \
		P_GET_PROPERTY(UStrProperty,Z_Param_ImagePath); \
		P_GET_UBOOL(Z_Param_SRGB); \
		P_GET_UBOOL(Z_Param_bClearBackground); \
		P_GET_STRUCT(FLinearColor,Z_Param_ClearColor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=UOdysseyBPLibrary::ThumbnailFromSceneCaptureComponent(Z_Param_SceneCaptureComponent,Z_Param_ImagePath,Z_Param_SRGB,Z_Param_bClearBackground,Z_Param_ClearColor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execLoadTexture2DFromFile) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_FullFilePath); \
		P_GET_UBOOL_REF(Z_Param_Out_IsValid); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_Width); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_Height); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(UTexture2D**)Z_Param__Result=UOdysseyBPLibrary::LoadTexture2DFromFile(Z_Param_FullFilePath,Z_Param_Out_IsValid,Z_Param_Out_Width,Z_Param_Out_Height); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execScreenShotWithName) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Name); \
		P_GET_PROPERTY(UIntProperty,Z_Param_ResolutionWidth); \
		P_GET_PROPERTY(UIntProperty,Z_Param_ResolutionHeight); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		UOdysseyBPLibrary::ScreenShotWithName(Z_Param_WorldContextObject,Z_Param_Name,Z_Param_ResolutionWidth,Z_Param_ResolutionHeight); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetMousePosition) \
	{ \
		P_GET_OBJECT(APlayerController,Z_Param_PlayerController); \
		P_GET_STRUCT(FVector2D,Z_Param_NewPos); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=UOdysseyBPLibrary::SetMousePosition(Z_Param_PlayerController,Z_Param_NewPos); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execFindNearestVertexLocationInRange) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject); \
		P_GET_STRUCT(FVector,Z_Param_Location); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Range); \
		P_GET_TARRAY(TEnumAsByte<EObjectTypeQuery>,Z_Param_ObjectTypes); \
		P_GET_TARRAY_REF(AActor*,Z_Param_Out_ActorsToIgnore); \
		P_GET_STRUCT_REF(FVector,Z_Param_Out_VertexPosition); \
		P_GET_OBJECT_REF(AActor,Z_Param_Out_HostActor); \
		P_GET_UBOOL_REF(Z_Param_Out_Found); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		UOdysseyBPLibrary::FindNearestVertexLocationInRange(Z_Param_WorldContextObject,Z_Param_Location,Z_Param_Range,Z_Param_ObjectTypes,Z_Param_Out_ActorsToIgnore,Z_Param_Out_VertexPosition,Z_Param_Out_HostActor,Z_Param_Out_Found); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetMeshVertexes) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_Component); \
		P_GET_TARRAY_REF(FVector,Z_Param_Out_Vertexes); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=UOdysseyBPLibrary::GetMeshVertexes(Z_Param_Component,Z_Param_Out_Vertexes); \
		P_NATIVE_END; \
	}


#define ScorchedSpaceVR_Plugins_Odyssey_Source_Odyssey_Public_OdysseyBPLibrary_h_40_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetDeviceOrientation) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(uint8*)Z_Param__Result=UOdysseyBPLibrary::GetDeviceOrientation(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIsDeviceCharging) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=UOdysseyBPLibrary::IsDeviceCharging(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetDeviceBatteryLevel) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(uint8*)Z_Param__Result=UOdysseyBPLibrary::GetDeviceBatteryLevel(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execDayInWeek) \
	{ \
		P_GET_STRUCT(FDateTime,Z_Param_Date); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(uint8*)Z_Param__Result=UOdysseyBPLibrary::DayInWeek(Z_Param_Date); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIsLeapYear) \
	{ \
		P_GET_STRUCT(FDateTime,Z_Param_Date); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=UOdysseyBPLibrary::IsLeapYear(Z_Param_Date); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetSubPaths) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_Directory); \
		P_GET_TARRAY_REF(FString,Z_Param_Out_SubPathsStripped); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		UOdysseyBPLibrary::GetSubPaths(Z_Param_Directory,Z_Param_Out_SubPathsStripped); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execClearDirectory) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_Directory); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=UOdysseyBPLibrary::ClearDirectory(Z_Param_Directory); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execUserDesktopFolder) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=UOdysseyBPLibrary::UserDesktopFolder(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execProjectSavedFolder) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=UOdysseyBPLibrary::ProjectSavedFolder(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetOverlappingWidgets) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject); \
		P_GET_OBJECT(UWidget,Z_Param_Widget); \
		P_GET_TARRAY(UWidget*,Z_Param_InWidgets); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(TArray<UWidget*>*)Z_Param__Result=UOdysseyBPLibrary::GetOverlappingWidgets(Z_Param_WorldContextObject,Z_Param_Widget,Z_Param_InWidgets); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetChildrenOfWidgetClass) \
	{ \
		P_GET_OBJECT(UWidget,Z_Param_ParentWidget); \
		P_GET_TARRAY_REF(UUserWidget*,Z_Param_Out_ChildWidgets); \
		P_GET_OBJECT(UClass,Z_Param_WidgetClass); \
		P_GET_UBOOL(Z_Param_bImmediateOnly); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		UOdysseyBPLibrary::GetChildrenOfWidgetClass(Z_Param_ParentWidget,Z_Param_Out_ChildWidgets,Z_Param_WidgetClass,Z_Param_bImmediateOnly); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetFirstWidgetOfClassPure) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject); \
		P_GET_OBJECT(UClass,Z_Param_WidgetClass); \
		P_GET_UBOOL(Z_Param_TopLevelOnly); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(UUserWidget**)Z_Param__Result=UOdysseyBPLibrary::GetFirstWidgetOfClassPure(Z_Param_WorldContextObject,Z_Param_WidgetClass,Z_Param_TopLevelOnly); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetFirstWidgetOfClass) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject); \
		P_GET_OBJECT(UClass,Z_Param_WidgetClass); \
		P_GET_UBOOL(Z_Param_TopLevelOnly); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(UUserWidget**)Z_Param__Result=UOdysseyBPLibrary::GetFirstWidgetOfClass(Z_Param_WorldContextObject,Z_Param_WidgetClass,Z_Param_TopLevelOnly); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execExplodeString) \
	{ \
		P_GET_TARRAY_REF(FString,Z_Param_Out_OutputStrings); \
		P_GET_PROPERTY(UStrProperty,Z_Param_InputString); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Separator); \
		P_GET_PROPERTY(UIntProperty,Z_Param_limit); \
		P_GET_UBOOL(Z_Param_bTrimElements); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		UOdysseyBPLibrary::ExplodeString(Z_Param_Out_OutputStrings,Z_Param_InputString,Z_Param_Separator,Z_Param_limit,Z_Param_bTrimElements); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execDistanceToString) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Value); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Exactness); \
		P_GET_UBOOL(Z_Param_bInRetardedUnits); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=UOdysseyBPLibrary::DistanceToString(Z_Param_Value,Z_Param_Exactness,Z_Param_bInRetardedUnits); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetFirstActorOfClassPure) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject); \
		P_GET_OBJECT(UClass,Z_Param_ActorClass); \
		P_GET_OBJECT_REF(AActor,Z_Param_Out_OutActor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		UOdysseyBPLibrary::GetFirstActorOfClassPure(Z_Param_WorldContextObject,Z_Param_ActorClass,Z_Param_Out_OutActor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetFirstActorOfClass) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject); \
		P_GET_OBJECT(UClass,Z_Param_ActorClass); \
		P_GET_OBJECT_REF(AActor,Z_Param_Out_OutActor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		UOdysseyBPLibrary::GetFirstActorOfClass(Z_Param_WorldContextObject,Z_Param_ActorClass,Z_Param_Out_OutActor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execFindOrSpawnTransform) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject); \
		P_GET_OBJECT(UClass,Z_Param_ActorClass); \
		P_GET_OBJECT_REF(AActor,Z_Param_Out_OutActor); \
		P_GET_STRUCT(FTransform,Z_Param_WithTransform); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		UOdysseyBPLibrary::FindOrSpawnTransform(Z_Param_WorldContextObject,Z_Param_ActorClass,Z_Param_Out_OutActor,Z_Param_WithTransform); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execFindOrSpawn) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject); \
		P_GET_OBJECT(UClass,Z_Param_ActorClass); \
		P_GET_OBJECT_REF(AActor,Z_Param_Out_OutActor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		UOdysseyBPLibrary::FindOrSpawn(Z_Param_WorldContextObject,Z_Param_ActorClass,Z_Param_Out_OutActor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execThumbnailFromSceneCaptureComponent) \
	{ \
		P_GET_OBJECT(USceneCaptureComponent2D,Z_Param_SceneCaptureComponent); \
		P_GET_PROPERTY(UStrProperty,Z_Param_ImagePath); \
		P_GET_UBOOL(Z_Param_SRGB); \
		P_GET_UBOOL(Z_Param_bClearBackground); \
		P_GET_STRUCT(FLinearColor,Z_Param_ClearColor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=UOdysseyBPLibrary::ThumbnailFromSceneCaptureComponent(Z_Param_SceneCaptureComponent,Z_Param_ImagePath,Z_Param_SRGB,Z_Param_bClearBackground,Z_Param_ClearColor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execLoadTexture2DFromFile) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_FullFilePath); \
		P_GET_UBOOL_REF(Z_Param_Out_IsValid); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_Width); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_Height); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(UTexture2D**)Z_Param__Result=UOdysseyBPLibrary::LoadTexture2DFromFile(Z_Param_FullFilePath,Z_Param_Out_IsValid,Z_Param_Out_Width,Z_Param_Out_Height); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execScreenShotWithName) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Name); \
		P_GET_PROPERTY(UIntProperty,Z_Param_ResolutionWidth); \
		P_GET_PROPERTY(UIntProperty,Z_Param_ResolutionHeight); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		UOdysseyBPLibrary::ScreenShotWithName(Z_Param_WorldContextObject,Z_Param_Name,Z_Param_ResolutionWidth,Z_Param_ResolutionHeight); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetMousePosition) \
	{ \
		P_GET_OBJECT(APlayerController,Z_Param_PlayerController); \
		P_GET_STRUCT(FVector2D,Z_Param_NewPos); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=UOdysseyBPLibrary::SetMousePosition(Z_Param_PlayerController,Z_Param_NewPos); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execFindNearestVertexLocationInRange) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject); \
		P_GET_STRUCT(FVector,Z_Param_Location); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Range); \
		P_GET_TARRAY(TEnumAsByte<EObjectTypeQuery>,Z_Param_ObjectTypes); \
		P_GET_TARRAY_REF(AActor*,Z_Param_Out_ActorsToIgnore); \
		P_GET_STRUCT_REF(FVector,Z_Param_Out_VertexPosition); \
		P_GET_OBJECT_REF(AActor,Z_Param_Out_HostActor); \
		P_GET_UBOOL_REF(Z_Param_Out_Found); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		UOdysseyBPLibrary::FindNearestVertexLocationInRange(Z_Param_WorldContextObject,Z_Param_Location,Z_Param_Range,Z_Param_ObjectTypes,Z_Param_Out_ActorsToIgnore,Z_Param_Out_VertexPosition,Z_Param_Out_HostActor,Z_Param_Out_Found); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetMeshVertexes) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_Component); \
		P_GET_TARRAY_REF(FVector,Z_Param_Out_Vertexes); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=UOdysseyBPLibrary::GetMeshVertexes(Z_Param_Component,Z_Param_Out_Vertexes); \
		P_NATIVE_END; \
	}


#define ScorchedSpaceVR_Plugins_Odyssey_Source_Odyssey_Public_OdysseyBPLibrary_h_40_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUOdysseyBPLibrary(); \
	friend struct Z_Construct_UClass_UOdysseyBPLibrary_Statics; \
public: \
	DECLARE_CLASS(UOdysseyBPLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Odyssey"), NO_API) \
	DECLARE_SERIALIZER(UOdysseyBPLibrary)


#define ScorchedSpaceVR_Plugins_Odyssey_Source_Odyssey_Public_OdysseyBPLibrary_h_40_INCLASS \
private: \
	static void StaticRegisterNativesUOdysseyBPLibrary(); \
	friend struct Z_Construct_UClass_UOdysseyBPLibrary_Statics; \
public: \
	DECLARE_CLASS(UOdysseyBPLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Odyssey"), NO_API) \
	DECLARE_SERIALIZER(UOdysseyBPLibrary)


#define ScorchedSpaceVR_Plugins_Odyssey_Source_Odyssey_Public_OdysseyBPLibrary_h_40_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOdysseyBPLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOdysseyBPLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOdysseyBPLibrary); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOdysseyBPLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOdysseyBPLibrary(UOdysseyBPLibrary&&); \
	NO_API UOdysseyBPLibrary(const UOdysseyBPLibrary&); \
public:


#define ScorchedSpaceVR_Plugins_Odyssey_Source_Odyssey_Public_OdysseyBPLibrary_h_40_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOdysseyBPLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOdysseyBPLibrary(UOdysseyBPLibrary&&); \
	NO_API UOdysseyBPLibrary(const UOdysseyBPLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOdysseyBPLibrary); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOdysseyBPLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOdysseyBPLibrary)


#define ScorchedSpaceVR_Plugins_Odyssey_Source_Odyssey_Public_OdysseyBPLibrary_h_40_PRIVATE_PROPERTY_OFFSET
#define ScorchedSpaceVR_Plugins_Odyssey_Source_Odyssey_Public_OdysseyBPLibrary_h_35_PROLOG
#define ScorchedSpaceVR_Plugins_Odyssey_Source_Odyssey_Public_OdysseyBPLibrary_h_40_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ScorchedSpaceVR_Plugins_Odyssey_Source_Odyssey_Public_OdysseyBPLibrary_h_40_PRIVATE_PROPERTY_OFFSET \
	ScorchedSpaceVR_Plugins_Odyssey_Source_Odyssey_Public_OdysseyBPLibrary_h_40_RPC_WRAPPERS \
	ScorchedSpaceVR_Plugins_Odyssey_Source_Odyssey_Public_OdysseyBPLibrary_h_40_INCLASS \
	ScorchedSpaceVR_Plugins_Odyssey_Source_Odyssey_Public_OdysseyBPLibrary_h_40_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ScorchedSpaceVR_Plugins_Odyssey_Source_Odyssey_Public_OdysseyBPLibrary_h_40_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ScorchedSpaceVR_Plugins_Odyssey_Source_Odyssey_Public_OdysseyBPLibrary_h_40_PRIVATE_PROPERTY_OFFSET \
	ScorchedSpaceVR_Plugins_Odyssey_Source_Odyssey_Public_OdysseyBPLibrary_h_40_RPC_WRAPPERS_NO_PURE_DECLS \
	ScorchedSpaceVR_Plugins_Odyssey_Source_Odyssey_Public_OdysseyBPLibrary_h_40_INCLASS_NO_PURE_DECLS \
	ScorchedSpaceVR_Plugins_Odyssey_Source_Odyssey_Public_OdysseyBPLibrary_h_40_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ScorchedSpaceVR_Plugins_Odyssey_Source_Odyssey_Public_OdysseyBPLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
