// Copyright 2017-2018 Immeractive. All Rights Reserved.

#pragma once

#include "Engine.h"
#include "SharedPointer.h"
#include "Widget.h"
#include "UserWidget.h"
#include "WidgetTree.h"
#include "IImageWrapper.h"
#include "IImageWrapperModule.h"
#include "HighResScreenshot.h"
#include "Runtime/Engine/Classes/Engine/StaticMesh.h"

#include "OdysseyBPLibrary.generated.h"

/*
	Function library class.
	Each function in it is expected to be static and represents blueprint node that can be called in any blueprint.

	When declaring function you can define metadata for the node. Key function specifiers will be BlueprintPure and BlueprintCallable.
	BlueprintPure - means the function does not affect the owning object in any way and thus creates a node without Exec pins.
	BlueprintCallable - makes a function which can be executed in Blueprints - Thus it has Exec pins.
	DisplayName - full name of the node, shown when you mouse over the node and in the blueprint drop down menu.
				Its lets you name the node using characters not allowed in C++ function names.
	CompactNodeTitle - the word(s) that appear on the node.
	Keywords -	the list of keywords that helps you to find node when you search for it using Blueprint drop-down menu.
				Good example is "Print String" node which you can find also by using keyword "log".
	Category -	the category your node will be under in the Blueprint drop-down menu.

	For more info on custom blueprint nodes visit documentation:
	https://wiki.unrealengine.com/Custom_Blueprint_Node_Creation
*/

UCLASS()
class UOdysseyBPLibrary : public UBlueprintFunctionLibrary
{

public:
	GENERATED_BODY()
public:

#pragma region Utilities
	/** Obtain the scaled,rotated, and translated vertex positions for any static mesh! Returns false if operation could not occur because the comp or static mesh asset was invalid. */
	UFUNCTION(BlueprintPure, Category = "Utilities")
		static bool GetMeshVertexes(UPrimitiveComponent* Component, TArray<FVector>& Vertexes);

	/*Returns the location of the nearest vertex to the Input location with a range.*/
	UFUNCTION(BlueprintCallable, Category = "Utilities", meta = (WorldContext = "WorldContextObject", Keywords = "Find Vertex Range"))
		static void FindNearestVertexLocationInRange(UObject * WorldContextObject, FVector Location, float Range, const TArray<TEnumAsByte<EObjectTypeQuery> > ObjectTypes, const TArray<AActor*>& ActorsToIgnore, FVector& VertexPosition, AActor*& HostActor, bool& Found);

	/*Sets the mouse position in screen space.*/
	UFUNCTION(BlueprintCallable, Category = "Utilities", meta = (DisplayName = "Set Mouse Cursor Position", Keywords = "Mouse Cursor Position"))
		static bool SetMousePosition(APlayerController* PlayerController, FVector2D NewPos);
#pragma endregion

#pragma region Images
	/*Takes a highres screenshot in the default location with a name.*/
	UFUNCTION(BlueprintCallable, Category = "Images", meta = (WorldContext = "WorldContextObject"))
		static void ScreenShotWithName(const UObject * WorldContextObject, const FString& Name, int32 ResolutionWidth, int32 ResolutionHeight);

	/*Loads a texture from an absolute filepath.*/
	UFUNCTION(BlueprintCallable, Category = "Images")
		static UTexture2D * LoadTexture2DFromFile(const FString & FullFilePath, bool & IsValid, int32 & Width, int32 & Height);

	/*Generates a thumbnail from a SceneCaptureComponent on an absolute location.*/
	UFUNCTION(BlueprintCallable, Category = "Images", Meta = (Keywords = "Thumbnail Capture"))
		static bool ThumbnailFromSceneCaptureComponent(USceneCaptureComponent2D * SceneCaptureComponent, const FString ImagePath, bool SRGB, bool bClearBackground = true, FLinearColor ClearColor = FLinearColor(1, 1, 1, 1));
#pragma endregion

#pragma region Actor Accessors
	/*Try to find an object, if it fails, spawn it instead.*/
	UFUNCTION(BlueprintCallable, Category = "Utilities", meta = (WorldContext = "WorldContextObject", DeterminesOutputType = "ActorClass", DynamicOutputParam = "OutActor", UnsafeDuringActorConstruction))
		static void FindOrSpawn(const UObject * WorldContextObject, TSubclassOf<AActor> ActorClass, AActor*& OutActor);

	/*Try to find an object, if it fails, spawn it instead with added transform.*/
	UFUNCTION(BlueprintCallable, Category = "Utilities", meta = (WorldContext = "WorldContextObject", DeterminesOutputType = "ActorClass", DynamicOutputParam = "OutActor", UnsafeDuringActorConstruction))
		static void FindOrSpawnTransform(const UObject * WorldContextObject, TSubclassOf<AActor> ActorClass, AActor*& OutActor, FTransform WithTransform);

	/*Returns the first actor of requested class.*/
	UFUNCTION(BlueprintCallable, Category = "Utilities", meta = (WorldContext = "WorldContextObject", DeterminesOutputType = "ActorClass", DynamicOutputParam = "OutActor"))
		static void GetFirstActorOfClass(const UObject* WorldContextObject, TSubclassOf<AActor> ActorClass, AActor*& OutActor);

	/*Returns the first actor of requested class. Pure version.*/
	UFUNCTION(BlueprintPure, Category = "Utilities", meta = (WorldContext = "WorldContextObject", DeterminesOutputType = "ActorClass", DynamicOutputParam = "OutActor"))
		static void GetFirstActorOfClassPure(const UObject* WorldContextObject, TSubclassOf<AActor> ActorClass, AActor*& OutActor);


#pragma endregion

#pragma region String Expansion
	/*Converts a raw float value to a string witch m, cm or km appended depending on the length.*/
	UFUNCTION(BlueprintCallable, Category = "String", meta = (Keywords = "Get String with Units for distance."))
		static FString DistanceToString(float Value, int32 Exactness = 2, bool bInRetardedUnits = false);

	/*Converts a formatted string with separator into different strings without separator.*/
	UFUNCTION(BlueprintPure, Category = "String", meta = (DisplayName = "Get Strings With Separator", Keywords = "Get Strings With Separator"))
		static void ExplodeString(TArray<FString>& OutputStrings, FString InputString, FString Separator = ",", int32 limit = 0, bool bTrimElements = false);
#pragma endregion

#pragma region Widget Blueprint Nodes
	/*Returns the first widget of the requested class*/
	UFUNCTION(BlueprintCallable, Category = "Widget", BlueprintCosmetic, Meta = (WorldContext = "WorldContextObject", DeterminesOutputType = "WidgetClass"))
		static UUserWidget* GetFirstWidgetOfClass(UObject* WorldContextObject, TSubclassOf<UUserWidget> WidgetClass, bool TopLevelOnly);

	/*Returns the first widget of the requested class. Pure version*/
	UFUNCTION(BlueprintPure, Category = "Widget", BlueprintCosmetic, Meta = (WorldContext = "WorldContextObject", DeterminesOutputType = "WidgetClass"))
		static UUserWidget* GetFirstWidgetOfClassPure(UObject* WorldContextObject, TSubclassOf<UUserWidget> WidgetClass, bool TopLevelOnly);

	/*Returns the children of the requested class*/
	UFUNCTION(BlueprintCallable, Category = "Widget", BlueprintCosmetic, Meta = (DefaultToSelf = "ParentWidget", DeterminesOutputType = "WidgetClass", DynamicOutputParam = "ChildWidgets"))
		static void GetChildrenOfWidgetClass(UWidget* ParentWidget, TArray<UUserWidget*>& ChildWidgets, TSubclassOf<UUserWidget> WidgetClass, bool bImmediateOnly);

	/*Returns all overlapping widgets*/
	UFUNCTION(BlueprintPure, Category = "Widget", meta = (HidePin = "WorldContextObject", DefaultToSelf = "WorldContextObject"))
		static TArray<UWidget*> GetOverlappingWidgets(UObject* WorldContextObject, UWidget* Widget, TArray<UWidget*> InWidgets);
#pragma endregion

#pragma region Directories
	/*Returns the location of the Saved folder (where images etc are stored)*/
	UFUNCTION(BlueprintPure, Category = "Directories")
		static FString ProjectSavedFolder();

	/*Returns the location of the User Desktop folder*/
	UFUNCTION(BlueprintPure, Category = "Directories")
		static FString UserDesktopFolder();

	/*Clears anything located in the folder, USE WITH CAUTION!*/
	UFUNCTION(BlueprintCallable, Category = "Directories")
		static bool ClearDirectory(const FString Directory);

	/*Returns a structured list of all subfolders in the called folder, will also return stripped folder names*/
	UFUNCTION(BlueprintCallable, Category = "Directories")
		static void GetSubPaths(const FString Directory, TArray<FString>& SubPathsStripped);
#pragma endregion

#pragma region Dates
	/*Inputs a date and returns wheter or not it is a leap year*/
	UFUNCTION(BlueprintPure, Category = "Dates")
		static bool IsLeapYear(FDateTime Date) { return FDateTime::IsLeapYear(Date.GetYear()); };

	/*Gets the day of the week for a given date, 0 is Monday, 6 is Sunday*/
	UFUNCTION(BlueprintPure, Category = "Dates")
		static uint8 DayInWeek(FDateTime Date);
#pragma endregion

#pragma region Mobile
	/*Gets the battery level, only works for Android/iOS*/
	UFUNCTION(BlueprintPure, Category = "Mobile")
		static uint8 GetDeviceBatteryLevel();

	/*Returns if the mobile is charging*/
	UFUNCTION(BlueprintPure, Category = "Mobile")
		static bool IsDeviceCharging();

	/*0 is Unknown or unusable, 1 is portrait, 2 is landscape*/
	UFUNCTION(BlueprintPure, Category = "Mobile", meta=(DeprecatedFunction))
		static uint8 GetDeviceOrientation();
#pragma endregion
};
