// Copyright 2017-2018 Immeractive. All Rights Reserved.

#pragma once

#include "ModuleManager.h"

//General Log
DECLARE_LOG_CATEGORY_EXTERN(OdysseyLog, Log, All);

class FOdysseyModule : public IModuleInterface
{
public:

	/** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;
};