// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "OdysseyPrivatePCH.h"

//#include "MovieSceneCaptureModule.h"
//#include "MovieSceneCaptureProtocolRegistry.h"
//#include "OdysseyPanoramicVideoCapture.h"

#define LOCTEXT_NAMESPACE "FOdysseyModule"



void FOdysseyModule::StartupModule()
{
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module
	

/*#if WITH_EDITOR
	FMovieSceneCaptureProtocolRegistry& ProtocolRegistry = IMovieSceneCaptureModule::Get().GetProtocolRegistry();

	FMovieSceneCaptureProtocolInfo Info;
	Info.DisplayName = LOCTEXT("PanoramicVideoDescription", "Panoramic Video");
	Info.SettingsClassType = UPanoramicVideoCaptureSettings::StaticClass();
	Info.Factory = []() -> TSharedRef<IMovieSceneCaptureProtocol> {
		return MakeShareable(new FPanoramicVideoCaptureProtocol());
	};
	ProtocolRegistry.RegisterProtocol(TEXT("PanoramicVideo"), Info);
#endif*/
}

void FOdysseyModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.
	
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FOdysseyModule, Odyssey)

//General Log
DEFINE_LOG_CATEGORY(OdysseyLog);