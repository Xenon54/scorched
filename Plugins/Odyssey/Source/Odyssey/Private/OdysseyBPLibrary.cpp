// Copyright 2017-2018 Immeractive. All Rights Reserved.

#include "OdysseyPrivatePCH.h"
#include "OdysseyBPLibrary.h"
#include <ctime>
#include <iostream>

#if PLATFORM_ANDROID
#include "Runtime/Core/Public/Android/AndroidMisc.h"
#elif PLATFORM_IOS
#include "Runtime/Core/Public/IOS/IOSPlatformMisc.h"
#endif

#if (WITH_PHYSX && PLATFORM_WINDOWS)

////Body Setup
#include "PhysicsEngine/BodySetup.h"
//
//
////~~~ PhysX ~~~
#include "PhysXIncludes.h"
#include "PhysXPublic.h"		//For the ptou conversions

#endif

bool UOdysseyBPLibrary::SetMousePosition(APlayerController* PlayerController, FVector2D NewPos)
{
	if (!PlayerController) return false;
	//~~~~~~~~~~~~~

	//Get Player
	const ULocalPlayer * Player = Cast<ULocalPlayer>(PlayerController->Player);

	if (!Player) return false;

	//get view port ptr
	const UGameViewportClient * ViewportClient =
		Cast < UGameViewportClient >(Player->ViewportClient);

	if (!ViewportClient) return false;

	FViewport * Viewport = ViewportClient->Viewport;

	if (!Viewport) return false;

	//Set Mouse
	Viewport->SetMouse(int32(NewPos.X), int32(NewPos.Y));

	return true;
}


bool UOdysseyBPLibrary::GetMeshVertexes(UPrimitiveComponent* Component, TArray<FVector>& Vertexes)
{
#if (WITH_PHYSX && PLATFORM_WINDOWS)
	//Component Transform
	FTransform RV_Transform = Component->GetComponentTransform();

	//Body Setup valid?
	UBodySetup* BodySetup = Component->GetBodySetup();

	if (!BodySetup || !BodySetup->IsValidLowLevel())
	{
		return false;
	}

	for (physx::PxTriangleMesh* EachTriMesh : BodySetup->TriMeshes)
	{
		if (!EachTriMesh)
		{
			return false;
		}
		//~~~~~~~~~~~~~~~~

		//Number of vertices
		physx::PxU32 VertexCount = EachTriMesh->getNbVertices();

		//Vertex array
		const  physx::PxVec3* Vertices = EachTriMesh->getVertices();

		//For each vertex, transform the position to match the component Transform 
		for (physx::PxU32 v = 0; v < VertexCount; v++)
		{
			Vertexes.Add(RV_Transform.TransformPosition(P2UVector(Vertices[v])));
		}
	}
#endif

	return false;
}

void UOdysseyBPLibrary::FindNearestVertexLocationInRange(UObject * WorldContextObject, FVector Location, float Range, const TArray<TEnumAsByte<EObjectTypeQuery> > ObjectTypes, const TArray<AActor*>& ActorsToIgnore, FVector& VertexPosition, AActor*& HostActor, bool& Found)
{
	Found = false;
#if (WITH_PHYSX && PLATFORM_WINDOWS)
	UPrimitiveComponent* LocalBestComp = nullptr;
	FVector LocalBestVertex = FVector(MAX_FLT, MAX_FLT, MAX_FLT);
	float LocalBestDist = Range;

	TArray<AActor*> OutActors;
	UWorld* World = GEngine->GetWorldFromContextObjectChecked(WorldContextObject);

	// We do nothing if not class provided, rather than giving ALL actors!
	if (World)
	{
		OutActors.Empty();

		TArray<UPrimitiveComponent*> OverlapComponents;
		bool bOverlapped = UKismetSystemLibrary::SphereOverlapComponents(WorldContextObject, Location, Range, ObjectTypes, NULL, ActorsToIgnore, OverlapComponents);
		if (bOverlapped)
		{
			TArray<FVector> ComponentVertices;
			for (auto iComponent : OverlapComponents)
			{
				GetMeshVertexes(iComponent, ComponentVertices);

				for (auto iVertex : ComponentVertices)
				{
					float VertexDistance = FVector::Dist(iVertex, Location);
					if (VertexDistance < LocalBestDist)
					{
						LocalBestDist = VertexDistance;
						LocalBestVertex = iVertex;
						LocalBestComp = iComponent;
					}
				}
			}
		}
	}

	Found = LocalBestComp != nullptr;
	if (LocalBestComp)
	{
		HostActor = LocalBestComp->GetOwner();
		VertexPosition = LocalBestVertex;
	}
	else
	{
		VertexPosition = Location;
	}


	/*
	//See this wiki for more info on getting triangles
	//		https://wiki.unrealengine.com/Accessing_mesh_triangles_and_vertex_positions_in_build
	*/

#else
	Found = false;
#endif

}

void UOdysseyBPLibrary::ScreenShotWithName(const UObject * WorldContextObject, const FString& Name, int32 ResolutionWidth = 3840, int32 ResolutionHeight = 2160)
{
	if (GEngine) {
		FHighResScreenshotConfig &HighResScreenshotConfig = GetHighResScreenshotConfig();
		HighResScreenshotConfig.FilenameOverride = Name + ".png";
		HighResScreenshotConfig.SetResolution(ResolutionWidth, ResolutionHeight);
	}
}

void UOdysseyBPLibrary::FindOrSpawn(const UObject * WorldContextObject, TSubclassOf<AActor> ActorClass, AActor*& OutActor)
{
	FindOrSpawnTransform(WorldContextObject, ActorClass, OutActor, FTransform());
}

void UOdysseyBPLibrary::FindOrSpawnTransform(const UObject * WorldContextObject, TSubclassOf<AActor> ActorClass, AActor*& OutActor, FTransform WithTransform)
{
	UWorld* World = GEngine->GetWorldFromContextObjectChecked(WorldContextObject);
	if (ActorClass && World)
	{
		for (TActorIterator<AActor> It(World, ActorClass); It; ++It)
		{
			AActor* Actor = *It;
			if (!Actor->IsPendingKill())
			{
				OutActor = Actor;
				//if it finds the actor, the actor will be assigned to the given value (reference). The function will stop then
				return;
			}
		}
		//if it fails to find the actor, it will create the actor
		FVector Location = WithTransform.GetLocation();
		FRotator Rotation = WithTransform.GetRotation().Rotator();

		FActorSpawnParameters SpawnInfo;
		SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
		//spawn an actor of the chosen class (ActorClass)
		OutActor = World->SpawnActor<AActor>(ActorClass, Location, Rotation, SpawnInfo);
	}
}

void UOdysseyBPLibrary::GetFirstActorOfClass(const UObject * WorldContextObject, TSubclassOf<AActor> ActorClass, AActor*& OutActor)
{

	UWorld* World = GEngine->GetWorldFromContextObjectChecked(WorldContextObject);

	// We do nothing if there's no class provided, rather than giving ALL actors!
	if (ActorClass && World)
	{
		for (TActorIterator<AActor> It(World, ActorClass); It; ++It)
		{
			AActor* Actor = *It;
			if (!Actor->IsPendingKill())
			{
				OutActor = Actor;
				break;
			}
		}
	}
}

void UOdysseyBPLibrary::GetFirstActorOfClassPure(const UObject * WorldContextObject, TSubclassOf<AActor> ActorClass, AActor *& OutActor)
{
	GetFirstActorOfClass(WorldContextObject, ActorClass, OutActor);
}


UTexture2D* UOdysseyBPLibrary::LoadTexture2DFromFile(const FString& FullFilePath, bool& IsValid, int32& Width, int32& Height)
{
	IsValid = FPaths::FileExists(FullFilePath);
	//if no . is found that means there's no extension or it is appended to the name, returning an invalid massage rather than an empty file.
	if (!FullFilePath.Contains(".") || !IsValid) return nullptr;

	UTexture2D* LoadedT2D = nullptr;
	IImageWrapperModule& ImageWrapperModule = FModuleManager::LoadModuleChecked<IImageWrapperModule>(FName("ImageWrapper"));
	TSharedPtr<IImageWrapper> ImageWrapper = ImageWrapperModule.CreateImageWrapper(EImageFormat::PNG);

	//Load From File
	TArray<uint8> RawFileData;

	if (!FFileHelper::LoadFileToArray(RawFileData, *FullFilePath)) return nullptr;
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	//Create T2D!
	if (ImageWrapper.IsValid() && ImageWrapper->SetCompressed(RawFileData.GetData(), RawFileData.Num()))
	{
		const TArray<uint8>* UncompressedBGRA = nullptr;
		if (ImageWrapper->GetRaw(ERGBFormat::BGRA, 8, UncompressedBGRA))
		{
			LoadedT2D = UTexture2D::CreateTransient(ImageWrapper->GetWidth(), ImageWrapper->GetHeight(), EPixelFormat::PF_B8G8R8A8);

			//Valid?
			if (!LoadedT2D) return nullptr;
			//~~~~~~~~~~~~~~

			//Out!
			Width = ImageWrapper->GetWidth();
			Height = ImageWrapper->GetHeight();

			//Copy!
			void* TextureData = LoadedT2D->PlatformData->Mips[0].BulkData.Lock(LOCK_READ_WRITE);
			FMemory::Memcpy(TextureData, UncompressedBGRA->GetData(), UncompressedBGRA->Num());
			LoadedT2D->PlatformData->Mips[0].BulkData.Unlock();

			//Update!
			LoadedT2D->UpdateResource();
		}
	}

	// Success!
	return LoadedT2D;
}

bool UOdysseyBPLibrary::ThumbnailFromSceneCaptureComponent(class USceneCaptureComponent2D* SceneCaptureComponent, const FString ImagePath, bool sRGB, bool bClearBackground, FLinearColor ClearColor)
{
	if (!SceneCaptureComponent || !(SceneCaptureComponent->TextureTarget))
	{
		return false;
	}

	FRenderTarget* RenderTarget = SceneCaptureComponent->TextureTarget->GameThread_GetRenderTargetResource();
	if (!RenderTarget)
	{
		return false;
	}

	TArray<FColor> RawPixels;

	// Format not supported - use PF_B8G8R8A8.
	if (SceneCaptureComponent->TextureTarget->GetFormat() != PF_B8G8R8A8 || !RenderTarget->ReadPixels(RawPixels))
	{
		return false;
	}

	// Convert to FColor.
	FColor ClearFColor = ClearColor.ToFColor(sRGB);

	for (FColor& Pixel : RawPixels)
	{
		// Switch Red/Blue changes.
		const uint8 PR = Pixel.R;
		const uint8 PB = Pixel.B;
		Pixel.R = PB;
		Pixel.B = PR;

		// Set alpha based on RGB values of ClearColor.
		if (bClearBackground)
			Pixel.A = ((Pixel.R == ClearFColor.R) && (Pixel.G == ClearFColor.G) && (Pixel.B == ClearFColor.B)) ? 0 : 255;
	}
	IImageWrapperModule& ImageWrapperModule = FModuleManager::LoadModuleChecked<IImageWrapperModule>(FName("ImageWrapper"));
	TSharedPtr<IImageWrapper> ImageWrapper = ImageWrapperModule.CreateImageWrapper(EImageFormat::PNG);

	const int32 Width = SceneCaptureComponent->TextureTarget->SizeX;
	const int32 Height = SceneCaptureComponent->TextureTarget->SizeY;

	if (ImageWrapper.IsValid() && ImageWrapper->SetRaw(&RawPixels[0], RawPixels.Num() * sizeof(FColor), Width, Height, ERGBFormat::RGBA, 8))
	{
		FFileHelper::SaveArrayToFile(ImageWrapper->GetCompressed(0), *ImagePath);
		return true;
	}

	return false;
}
inline bool WidgetIntersection(UWidget* A, UWidget* B)
{
	const FVector2D ASize = A->GetDesiredSize();
	const FVector2D APosition = A->RenderTransform.Translation;

	const FVector2D BSize = B->GetDesiredSize();
	const FVector2D BPosition = B->RenderTransform.Translation;

	return (FMath::Abs(APosition.X - BPosition.X) < (ASize.X + BSize.X) / 2.0f) &&
		(FMath::Abs(APosition.Y - BPosition.Y) < (ASize.Y + BSize.Y) / 2.0f);
}

TArray<UWidget*> UOdysseyBPLibrary::GetOverlappingWidgets(UObject * WorldContextObject, UWidget * Widget, TArray<UWidget*> InWidgets)
{
	TArray<UWidget*> OverlappingWidgets;
	//FVector2D TheSize = Widget->GetDesiredSize();
	FVector2D ThePosition = Widget->RenderTransform.Translation;

	if (Widget->IsValidLowLevel())
	{
		for (auto W : InWidgets)
		{
			if (W != Widget)
			{
				//FVector2D WSize = W->GetDesiredSize();
				FVector2D WPosition = W->RenderTransform.Translation;

				if (WidgetIntersection(Widget, W))
				{
					OverlappingWidgets.Add(W);
				}
			}
		}
	}

	return OverlappingWidgets;
}


FString UOdysseyBPLibrary::DistanceToString(float Value, int32 Exactness, bool bInRetardedUnits)
{
	float TempValue = FMath::Abs(Value);
	Value = FMath::GridSnap(Value, .01f);

	FNumberFormattingOptions Format;
	Format.MaximumFractionalDigits = Exactness;

	if (!bInRetardedUnits)
	{
		if (TempValue > 100000.f)
		{
			return FText::AsNumber(Value / 100000.f, &Format).ToString() + " km";
			//return (StringValue + " km");
		}
		else if (TempValue > 100.f)
		{
			return FText::AsNumber(Value / 100.f, &Format).ToString() + " m";
		}

		else
		{
			return FText::AsNumber(Value, &Format).ToString() + " cm";
		}
	}
	else
	{
		//Return Imperial Units.
		//conevert to base unit in. first.
		Value *= 0.3937007874f;
		if (TempValue > 5280.f)
		{
			return FText::AsNumber(Value / 5280.f, &Format).ToString() + " mi";
		}
		else if (TempValue > 24.f) //from 24in start displaying in ft
		{
			return FText::AsNumber(Value / 12.f, &Format).ToString() + " ft";
		}
		else
		{
			return FText::AsNumber(Value, &Format).ToString() + " in";
		}
	}
}

void UOdysseyBPLibrary::ExplodeString(TArray<FString>& OutputStrings, FString InputString, FString Separator, int32 limit, bool bTrimElements)
{
	OutputStrings.Empty();

	if (InputString.Len() > 0 && Separator.Len() > 0) {
		int32 StringIndex = 0;
		int32 SeparatorIndex = 0;

		FString Section = "";
		FString Extra = "";

		int32 PartialMatchStart = -1;

		while (StringIndex < InputString.Len()) {

			if (InputString[StringIndex] == Separator[SeparatorIndex]) {
				if (SeparatorIndex == 0) {
					//A new partial match has started.
					PartialMatchStart = StringIndex;
				}
				Extra.AppendChar(InputString[StringIndex]);
				if (SeparatorIndex == (Separator.Len() - 1)) {
					//We have matched the entire separator.
					SeparatorIndex = 0;
					PartialMatchStart = -1;
					if (bTrimElements == true) {
						OutputStrings.Add(FString(Section).TrimStart().TrimEnd());
					}
					else {
						OutputStrings.Add(FString(Section));
					}

					//if we have reached the limit, stop.
					if (limit > 0 && OutputStrings.Num() >= limit)
					{
						return;
						//~~~~
					}

					Extra.Empty();
					Section.Empty();
				}
				else {
					++SeparatorIndex;
				}
			}
			else {
				//Not matched.
				//We should revert back to PartialMatchStart+1 (if there was a partial match) and clear away extra.
				if (PartialMatchStart >= 0) {
					StringIndex = PartialMatchStart;
					PartialMatchStart = -1;
					Extra.Empty();
					SeparatorIndex = 0;
				}
				Section.AppendChar(InputString[StringIndex]);
			}

			++StringIndex;
		}

		//If there is anything left in Section or Extra. They should be added as a new entry.
		if (bTrimElements == true) {
			OutputStrings.Add(FString(Section + Extra).TrimStart().TrimEnd());
		}
		else {
			OutputStrings.Add(FString(Section + Extra));
		}

		Section.Empty();
		Extra.Empty();
	}
}


UUserWidget* UOdysseyBPLibrary::GetFirstWidgetOfClass(UObject* WorldContextObject, TSubclassOf<UUserWidget> WidgetClass, bool TopLevelOnly)
{
	if (!WidgetClass || !WorldContextObject)
	{
		return nullptr;
	}

	const UWorld* World = GEngine->GetWorldFromContextObject(WorldContextObject, EGetWorldErrorMode::ReturnNull);
	if (!World)
	{
		return nullptr;
	}

	UUserWidget* ResultWidget = nullptr;
	for (TObjectIterator<UUserWidget> Itr; Itr; ++Itr)
	{
		UUserWidget* LiveWidget = *Itr;

		// Skip any widget that's not in the current world context.
		if (LiveWidget->GetWorld() != World)
		{
			continue;
		}

		// Skip any widget that is not a child of the class specified.
		if (!LiveWidget->GetClass()->IsChildOf(WidgetClass))
		{
			continue;
		}

		if (!TopLevelOnly || LiveWidget->IsInViewport())
		{
			ResultWidget = LiveWidget;
			break;
		}
	}

	return ResultWidget;
}

UUserWidget * UOdysseyBPLibrary::GetFirstWidgetOfClassPure(UObject * WorldContextObject, TSubclassOf<UUserWidget> WidgetClass, bool TopLevelOnly)
{
	return GetFirstWidgetOfClass(WorldContextObject, WidgetClass, TopLevelOnly);
}

void UOdysseyBPLibrary::GetChildrenOfWidgetClass(UWidget* ParentWidget, TArray<UUserWidget*>& ChildWidgets, TSubclassOf<UUserWidget> WidgetClass, bool bImmediateOnly)
{
	if (ParentWidget && WidgetClass)
	{
		// Current set of widgets to check
		TInlineComponentArray<UWidget*> WidgetsToCheck;

		// Set of all widgets we have checked
		TInlineComponentArray<UWidget*> CheckedWidgets;

		WidgetsToCheck.Push(ParentWidget);

		// While still work left to do
		while (WidgetsToCheck.Num() > 0)
		{
			// Get the next widgets off the queue
			const bool bAllowShrinking = false;
			UWidget* PossibleParent = WidgetsToCheck.Pop(bAllowShrinking);

			// Add it to the 'checked' set, should not already be there!
			if (!CheckedWidgets.Contains(PossibleParent))
			{
				CheckedWidgets.Add(PossibleParent);

				TArray<UWidget*> Widgets;

				UWidgetTree::GetChildWidgets(PossibleParent, Widgets);

				for (UWidget* Widget : Widgets)
				{
					if (!CheckedWidgets.Contains(Widget))
					{
						// Add any widget that is a child of the class specified.
						if (Widget->GetClass()->IsChildOf(WidgetClass))
						{
							ChildWidgets.Add(Cast<UUserWidget>(Widget));
						}

						// If we're not just looking for our immediate children,
						// add this widget to list of widgets to check next.
						if (!bImmediateOnly)
						{
							WidgetsToCheck.Push(Widget);
						}
					}
				}

				if (bImmediateOnly)
				{
					break;
				}
			}
		}
	}
}

FString UOdysseyBPLibrary::ProjectSavedFolder()
{
	return FPaths::ConvertRelativePathToFull(FPaths::ProjectSavedDir());
}

FString UOdysseyBPLibrary::UserDesktopFolder()
{
	return FString(FPlatformProcess::UserDir()).Replace(TEXT("Documents"), TEXT("Desktop"));
}

bool UOdysseyBPLibrary::ClearDirectory(const FString Directory)
{
	IPlatformFile& PlatformFile = FPlatformFileManager::Get().GetPlatformFile();

	if (PlatformFile.DirectoryExists(*Directory))
	{
		FJsonSerializableArray Files;
		PlatformFile.FindFiles(Files, *Directory, NULL);
		for (auto File : Files)
		{
			FPlatformFileManager::Get().GetPlatformFile().DeleteFile(*File);
		}
		return true;
	}
	return false;
}

void UOdysseyBPLibrary::GetSubPaths(const FString Directory, TArray<FString>& SubPathsStripped)
{
	TArray<FString> Directories;
	IFileManager &FileManager = IFileManager::Get();
	FString Dir = FPaths::ProjectDir() / "Content" / Directory / TEXT("*");
	FileManager.FindFiles(SubPathsStripped, *Dir, false, true);
}

uint8 UOdysseyBPLibrary::DayInWeek(FDateTime Date)
{
	return uint8(Date.GetDayOfWeek());
}

uint8 UOdysseyBPLibrary::GetDeviceBatteryLevel()
{
#if PLATFORM_ANDROID
	return uint8(FAndroidMisc::GetBatteryLevel());
#elif PLATFORM_IOS
	return uint8(FIOSPlatformMisc::GetBatteryLevel());
#endif
	return 255;
}

bool UOdysseyBPLibrary::IsDeviceCharging()
{
#if PLATFORM_ANDROID
	switch (FAndroidMisc::GetBatteryState().State)
	{
	case  FAndroidMisc::EBatteryState::BATTERY_STATE_CHARGING: return true; break;
	default: 	return false;
	}

#elif PLATFORM_IOS
	return !FIOSPlatformMisc::IsRunningOnBattery();
#endif
	return false;
}

uint8 UOdysseyBPLibrary::GetDeviceOrientation()
{
#if PLATFORM_ANDROID
	//return EScreenOrientation(FAndroidMisc::GetDeviceOrientation());
	return 0U;
#elif PLATFORM_IOS
	switch (FIOSPlatformMisc::GetDeviceOrientation())
	{
	case EDeviceScreenOrientation::Portrait:
		return 1U;
		break;
	case EDeviceScreenOrientation::PortraitUpsideDown:
		return 1U;
		break;
	case EDeviceScreenOrientation::LandscapeLeft:
		return 2U;
		break;
	case EDeviceScreenOrientation::LandscapeRight:
		return 2U;
		break;
	default:
		return 0U;
		break;
	}
#endif
	return 0U;
}


