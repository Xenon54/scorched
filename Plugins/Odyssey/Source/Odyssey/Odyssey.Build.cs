// Copyright 2017-2018 Immeractive. All Rights Reserved.

using UnrealBuildTool;

public class Odyssey : ModuleRules
{
	public Odyssey(ReadOnlyTargetRules Target) : base(Target)
	{
        PrivatePCHHeaderFile = "Private/OdysseyPrivatePCH.h";

        PublicIncludePaths.AddRange(
			new string[] {
				//"Odyssey/Public"
				// ... add public include paths required here ...
			}
		);
				
		
		PrivateIncludePaths.AddRange(
			new string[] {
			//	"Odyssey/Private",
				// ... add other private include paths required here ...
			}
		);
			
		
		PublicDependencyModuleNames.AddRange(
			new string[]
			{
                 "Engine"
				// ... add other public dependencies that you statically link with here ...
			}
		);

        

        PrivateDependencyModuleNames.AddRange(
			new string[]
			{
               
                "Core",
                "CoreUObject",
				"Engine",

				"Slate",
				"SlateCore",

                
                "WebBrowser",
               // "HTML5Networking",
                "AVIWriter",
                "UMG",
                "Json",
                "ImageWrapper",
                
                "RenderCore",

                "LevelSequence",
                "MovieScene",
                "MovieSceneTracks",
                "MovieSceneCapture"
				// ... add private dependencies that you statically link with here ...	
			}
		);

        if (Target.bBuildEditor)
        {
            PrivateDependencyModuleNames.Add("UnrealEd");
            PrivateDependencyModuleNames.Add("SequenceRecorder");
        }

        if ((Target.Platform == UnrealTargetPlatform.Win64) ||
            (Target.Platform == UnrealTargetPlatform.Win32))
        {
            PrivateDependencyModuleNames.Add("PhysX");
            PrivateDependencyModuleNames.Add("APEX");
        }
        //"PhysX", //"APEX",

        DynamicallyLoadedModuleNames.AddRange(
            new string[]
            {
                
				// ... add any modules that your module loads dynamically here ...
			}
			);
	}
}
